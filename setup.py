#coding: utf-8
from setuptools import setup
from distutils.command.sdist import sdist as _sdist

if __name__ == "__main__":
	import sys
	versao = sys.version_info.major*1 + sys.version_info.minor * 0.1
	if versao < 3.5:
		sys.exit('Para instalar o SimuS é necessário usar python com versão >= 3.5')
		
setup(
	name='SimuS',
	description = "Simulador do processador Sapiens",
	author="Daniel Barbosa Bruno",
	author_email="danielbbruno@proton.me",
	version='1.2.1',
	packages=["SimuS", "SimuS.interfaceDeUsuário", "SimuS.controlador", "SimuS.simulador", "SimuS.compilador", "SimuS.interfaceDeUsuário.designer"],
	#scripts=["scripts/simus",],
	entry_points={
		'gui_scripts':[
			'simus16 = SimuS.__main__:main'
		]
	},
	python_requires='>=3.5',
	install_requires=[
		"PyQt5>=5.8",
	],
	package_data={"":["interfaceDeUsuário/prefixo.html", "interfaceDeUsuário/folhasDeEstilo.qss", "interfaceDeUsuário/íconesFeatherBin.rcc"]},
	url = "https://gitlab.com/danielbbruno/SimuS",
)
