#!/bin/sh

cp SimuS/interfaceDeUsuário/íconesFeather.qrc pacotesExternos/feather/icons/íconesFeather.qrc
cd pacotesExternos/feather/icons

for arquivo in ./*
do
	if [[ "$arquivo" == *.svg ]]; then
		echo "$arquivo.png"
		inkscape -o "$arquivo.png" -w 64 -h 64 $arquivo
	fi
done

rcc -binary íconesFeather.qrc -o ../../../SimuS/interfaceDeUsuário/íconesFeatherBin.rcc
rm íconesFeather.qrc
rm *.png
