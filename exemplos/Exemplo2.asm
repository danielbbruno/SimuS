
; Exemplo 2
; Cálculo do n-ésimo termo da sequência de fibonacci usando recursão
;
			ORG 0
S1:			DW 1
S2:			DW 1
N:			DW 10
			
			

			ORG 10

inicio:		LDA N
			PUSH
			JSR funcaoFibo

			HLT
			
;Função que calcula o n-ésimo termo de fibonacci. Assume que na posição SP+2 esteja o argumento N da função.
			;Carrega N
funcaoFibo:	STS 
			ADD #2
			STA temp
			LDA @temp
			
			;Se N < 3, vá para a seção de casos base
			SUB #3
			JN casosBase
			
			;Caso contrário, chama a função com N - 2
			ADD #1
			PUSH
			JSR funcaoFibo
			;Armazena o resultado na variável resParcial
			STA resParcial
			;Armazena o valor de N - 2 em temp
			POP
			STA temp
			
			;Armazena o resultado parcial na pilha
			LDA resParcial
			PUSH
			
			;Chama a função com N - 1
			LDA temp
			ADD #1
			PUSH
			JSR funcaoFibo
			
			;Armazena o novo resultado em resParcial
			STA resParcial
			;Retira N - 1 da pilha
			POP
			;Retira o resultado antigo da pilha
			POP
			;Soma o resultado antigo ao novo, e retorna o resultado
			ADD resParcial
			RET

			;Retorna S1 se N = 1, e retorna S2 se N = 2
casosBase:	ADD #1 
			JNZ casoS1
			LDA S2
			RET

casoS1:		LDA S1
			RET



temp:		DW 0
resParcial: DW 0

			END 10

