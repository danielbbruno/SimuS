
; Exemplo 4
; Exemplo da rotina TRAP de número 6, para toque de som.
; Este programa toca a escala maior ascendente de Lá
; 
; A frequência tocada é dada por 
;	freq = 440* (2 ** ((altura/2 - 45)*(1/12)))
; isto é, altura 90 equivale a 440Hz, e a cada incrementos de 24 na altura, a frequência dobra

inicio:    	LDA pos
			SUB #16
			JZ fim
			LDA #altura
			ADD pos
			STA temp
			LDA @temp
			ADD duracao
			STA temp
			LDA #6
			TRAP temp
			LDA pos
			ADD #2
			STA pos
			JMP inicio
		
fim: 		HLT

altura: 	DW 90, 94, 98, 100, 104, 108, 112, 114
duracao:		DW 0xa00
pos: 		DW 0
temp:		DW 0
