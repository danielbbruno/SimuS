				ORG 0x1000
TXT_TESTE: 		STR "TESTE"
TXT_REVIVER:		STR "REVIVER"
TXT_EXEMPLO:		STR "EXEMPLO"

STR_É_PALIN:		STR "A cadeia de caracteres é palíndromo:"
STR_NÃO_É_PALIN:	STR "A cadeia de caracteres não é palíndromo:"

			ORG 0
			LDA #TXT_TESTE
			JSR F_RESULTADO

			REP
			LDA #TXT_REVIVER
			JSR F_RESULTADO

			REP
			LDA #TXT_EXEMPLO
			JSR F_RESULTADO

			HLT


PARAM_TXT:	DW 0
F_RESULTADO:	REP
			STA PARAM_TXT
			JSR F_PALINDR
			REP
			JZ CASO_POSIT

			LDA #4
			TRAP STR_NÃO_É_PALIN
			JMP FIM_RESULT

CASO_POSIT:	LDA #4
			TRAP STR_É_PALIN

FIM_RESULT:	LDA #4
			TRAP @PARAM_TXT
			RET
			

;Função que avalia se uma função é palíndromo
; - O parâmetro passado no acumulador deve ser o endereço contendo a string
;Variáveis:
END_ATUAL:		DW 0
END_INICIAL:		DW 0
NUM_DIFERENÇAS:	DB 0


;Início:
F_PALINDR:	REP
			STA END_INICIAL
			STA END_ATUAL
		
;Empilha cada caractere até encontrar o caractere nulo
LAÇO_UM:		SEP	
			LDA @END_ATUAL
			JZ PARTE_DOIS
			PUSH
			REP 
			LDA END_ATUAL
			ADD #1
			STA END_ATUAL
			JMP LAÇO_UM

;Armazena o endereço inicial da string na posição da memória END_ATUAL
; isso é feito apenas por razão de clareza, visto que END_INICIAL não é usado novamente na função
PARTE_DOIS:	REP
			LDA END_INICIAL
			STA END_ATUAL
			SEP

;Inicializa o contador de caracteres distintos
			LDA #0
			STA NUM_DIFERENÇAS

LAÇO_DOIS:	LDA @END_ATUAL
			JZ FIM
			POP
			SUB @END_ATUAL
			JZ FIM_L_DOIS

			LDA NUM_DIFERENÇAS
			ADD #1
			STA NUM_DIFERENÇAS

FIM_L_DOIS:	REP 
			LDA END_ATUAL
			ADD #1
			STA END_ATUAL
			SEP
			JMP LAÇO_DOIS

FIM:			LDA NUM_DIFERENÇAS
			RET