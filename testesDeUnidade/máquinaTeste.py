
import unittest
from unittest.mock import Mock
from SimuS.simulador import Máquina, Memória
from SimuS.simulador.exceções import *
from SimuS.simulador.parâmetros import endianness, tamanhoDaPalavra, tamanhoDoEndereço

class MáquinaCasoDeTeste(unittest.TestCase):
	"""Testes das instruções definidas na classe Máquina"""
	def setUp(self):
		memória = Memória(tamanhoDoEndereço = tamanhoDoEndereço, tamanhoDaPalavra = tamanhoDaPalavra, ordemDosBytes=endianness)
		self.máquinaInicial = Máquina(memória, cp=None)
		self.máquina = Máquina(memória, cp=None)
		
	def tearDown(self):
		del(self.máquina)
		del(self.máquinaInicial)

	def testeNOP(self,):
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)

	def testeSEP(self,):
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x04, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.modoOitoBits, True)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeREP(self,):
		dado = bytearray([0x08, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.modoOitoBits, False)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeSTA(self):
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x10, 0x04, 0x00])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.memória[3], 0x00)
		self.assertEqual(self.máquina.memória[4], 0xFA)
		self.assertEqual(self.máquina.memória[5], 0x0A)
		self.assertEqual(self.máquina.memória[6], 0x00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSTAind(self):
		self.máquina.acumulador = 0xCF3A
		dado = bytearray([0x11, 0x04, 0x00, 0xFF, 0x06, 0x00])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.memória[5], 0x00)
		self.assertEqual(self.máquina.memória[6], 0x3A)
		self.assertEqual(self.máquina.memória[7], 0xCF)
		self.assertEqual(self.máquina.memória[8], 0x00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeXGSP(self):
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0xF2FA
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0xAC23
		dado = bytearray([0x14, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeXGAC(self,):
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0xAC23
		dado = bytearray([0x18, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x23AC)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)

	def testeLDA(self):
		dado = bytearray([0x20, 0x04, 0x00, 0xFF, 0x7A, 0x12])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x127A)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeLDAind(self):
		dado = bytearray([0x21, 0x04, 0x00, 0xFF, 0x06, 0x00, 0x10, 0x01])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x0110)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeLDAimm(self):
		dado = bytearray([0x22, 0x00, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xC112
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADD(self):
		dado = bytearray([0x30, 0x04, 0x00, 0xFF, 0x0B])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x12
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 29)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADDimm(self):
		dado = bytearray([0x32, 0x5B, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x0012
		
		self.máquina.passo()

		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.acumulador, 109)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADDind(self):
		dado = bytearray([0x31, 0x06, 0x00, 0xFF, 0xCF, 0x01, 0x04, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x18A
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x359)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADC(self):
		dado = bytearray([0x34, 0x04, 0x00, 0xFF, 0x16])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x3A
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 81)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADCind(self):
		dado = bytearray([0x35, 0x04, 0x00, 0xFF, 0x06, 0x00, 0x00, 0xD5])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xEA00
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xbf00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
		
	def testeADCimm(self):
		dado = bytearray([0x36, 0xFF, 0x20, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xDF00
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSUB(self):
		dado = bytearray([0x38, 0x04, 0x00, 0xFF, 0x0A, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x12		
		self.carryFlag = True

		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 8)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSUBind(self):
		dado = bytearray([0x39, 0x06, 0x00, 0xFF, 0xBB, 0x00, 0x04, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xAA
		self.carryFlag = True

		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.acumulador, 0xffef)
		self.assertTrue(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
		
	def testeSUBimm(self):
		dado = bytearray([0x3A, 0x17, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x17
		self.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSBC(self):
		dado = bytearray([0x3c, 0x04, 0x00, 0xFF, 0x0A, 0x0b])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x0b17
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 11)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeSBCimm(self):
		dado = bytearray([0x3e, 0x03, 0x01, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x0b02
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x09ff)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeOR(self):
		dado = bytearray([0x40, 0x04, 0x00, 0xFF, 0x1B, 0x0f])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x100C
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x1f1f)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeXORimm(self):
		dado = bytearray([0x46, 0x2e, 0xf0, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xA912
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x593c)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeANDimm(self):
		dado = bytearray([0x52, 0xA2, 0x01])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x035D
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x0100)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeNOT(self):
		dado = bytearray([0x60, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x5D
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xFFA2)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSHL(self):
		dado = bytearray([0x70, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xE800
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xD000)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSHL2(self):
		dado = bytearray([0x70, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x4000
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x8000)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSHR(self):
		dado = bytearray([0x74, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xAE
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 87)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSRA(self):
		dado = bytearray([0x78, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x8070
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xc038)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSRA2(self):
		dado = bytearray([0x78, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x53
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 41)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeJMPind(self):
		dado = bytearray([0x81, 0x03, 0x00, 0xAE, 0x01])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x01AE)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJN(self):
		dado = bytearray([0x90, 0x08, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True		
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0008)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJN2(self):
		dado = bytearray([0x90, 0xAA, 0xCC])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJP(self):
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False				
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJP2(self):
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True				
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
			
	def testeJP3(self):
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False			
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0A08)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJZ(self):
		dado = bytearray([0xA0, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True			
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJZ2(self):
		dado = bytearray([0xA0, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0xAAAB, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJNZ(self):
		dado = bytearray([0xA4, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJNZ2(self):
		dado = bytearray([0xA4, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False	
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0xAAAB, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJC(self):
		dado = bytearray([0xb0, 0x00, 0x10 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x1000, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJC2(self):
		dado = bytearray([0xb0, 0x08, 0x00 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJNC(self):
		dado = bytearray([0xb4, 0x00, 0x10 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJNC2(self):
		dado = bytearray([0xb4, 0x08, 0x00 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0008, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJSR(self):
		dado = bytearray([0xd0, 0xAC, 0x10])
		end = 0x31FA
		self.máquina.memória[end:end + len(dado)] = dado
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0xFBFA
		self.máquina.apontadorDeInstrução = end
		
		self.máquina.passo()
		
		self.assertEqual(0x10AC, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha - 2)
		self.assertEqual(int.from_bytes(
				self.máquina.memória[
					self.máquina.apontadorDePilha:
					self.máquina.apontadorDePilha + tamanhoDaPalavra
				],
				self.máquina.memória.ordemDosBytes
				),
			len(dado) +end)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeRET(self):
		self.máquina.apontadorDePilha = 0xFFAC
		dado = bytearray([0xd8])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.memória[self.máquina.apontadorDePilha: self.máquina.apontadorDePilha + tamanhoDaPalavra] = bytearray((0x40, 0x00))
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDePilha, 0xFFAC + tamanhoDoEndereço )
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0040)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testePUSH(self):
		dado = bytearray([0xE0, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x0312
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0x00FF
		
		self.máquina.passo()
		
		self.assertEqual(
			int.from_bytes(
				self.máquina.memória[
					self.máquina.apontadorDePilha:
					self.máquina.apontadorDePilha + tamanhoDaPalavra
				],
				self.máquina.memória.ordemDosBytes
			)
			, 0x0312 
		)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha - tamanhoDaPalavra)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testePOP(self):
		dado = bytearray([0xE4, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0xFF
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0x0aFF
		self.máquina.memória[
			self.máquina.apontadorDePilha :
			self.máquina.apontadorDePilha + tamanhoDaPalavra
		] = bytearray((0x2a, 0x05))
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x052a)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha + tamanhoDaPalavra)
		self.assertEqual(self.máquina.carryFlag, False)
		self.assertEqual(self.máquina.negativeFlag, False)
		self.assertEqual(self.máquina.zeroFlag, False)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeTRAP1(self):
		dado = bytearray([0xF0, 0xFF, 0xFF, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x01
		self.máquina.leituraDoConsole = Mock(return_value=0x17)
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x17)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.leituraDoConsole.assert_called_with(cadeiaDeCaracteres=False)
	
	def testeTRAP2(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x76, 0xE2])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x02
		self.máquina.escritaNoConsole = Mock()
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.escritaNoConsole.assert_called_with(valor=0x76)
		
	def testeTRAP3(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x03
		self.máquina.leituraDoConsole = Mock(return_value=[0x17, 0x12, 0x76])
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.memória[4:7], bytearray([0x17, 0x12, 0x76]))
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.leituraDoConsole.assert_called_with(cadeiaDeCaracteres=True)
	
	def testeTRAP4(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x32, 0xE2, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x04
		self.máquina.escritaNoConsole = Mock()
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.escritaNoConsole.assert_called_with(valor=[0x32, 0xE2])
		
	def testeTRAP5(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x32, 0xE2])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x05
		self.máquina.esperaTempo = Mock()
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.esperaTempo.assert_called_with(valor=0xE232)
		
	def testeTRAP6(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x32, 0xE2, 0x61])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x06
		self.máquina.tocaSom = Mock()
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.tocaSom.assert_called_with(altura=0x32, duração=0xE2)
		
	def testeTRAP7(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x82, 0x12])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x07
		self.máquina.pseudoAleatório = Mock()
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)		
		self.máquina.pseudoAleatório.assert_called_with(semente=0x1282)
		
	def testeTRAPInexistente(self):
		dado = bytearray([0xF0, 0x04, 0x00, 0xFF, 0x82, 0x12])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x0e02
		self.máquina.pseudoAleatório = Mock()
		
		self.assertRaises(
			ExceçãoTRAPInválido,
			self.máquina.passo
		)
		
	def testeOpcodeInválido(self):
		dado = bytearray([0x66])
		self.máquina.memória[:len(dado)] = dado
		self.assertRaises(
			ExceçãoDeOpcodeInválido,
			self.máquina.passo
		)
		
	def testeOpcodeInválido2(self):
		dado = bytearray([0xFE])		
		self.máquina.memória[:len(dado)] = dado
		self.assertRaises(
			ExceçãoDeOpcodeInválido,
			self.máquina.passo
		)
	
	def testeOpcodeInválido3LDS(self):
		dado = bytearray([0x24])		
		self.máquina.memória[:len(dado)] = dado
		self.assertRaises(
			ExceçãoDeOpcodeInválido,
			self.máquina.passo
		)
		
	def testeTipoInválidoDeOperando(self):
		dado = bytearray([0xE5])		
		self.máquina.memória[:len(dado)] = dado
		self.assertRaises(
			ExceçãoDeOpcodeInválido,
			self.máquina.passo
		)