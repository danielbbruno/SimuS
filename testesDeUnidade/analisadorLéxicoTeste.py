
import unittest
from SimuS.compilador import AnalisadorLéxico, Símbolo
from SimuS.compilador.exceções import ExceçãoSímboloInválido

class AnalisadorLéxicoCasoDeTeste(unittest.TestCase):		
	def testeAnalisadorLéxico(self):
		texto = "ini:\tSUB var"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(
			tokens,
			[Símbolo("DefiniçãoDeRótulo", "INI"), Símbolo("Operador", "SUB"), Símbolo("Rótulo", "VAR"), Símbolo("EOF")]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxico2(self):
		texto = "loc:\tLDA var\n\tADD var\n\tSTA var\n\tSUB #100\n\tJN fim\n\tJN ini\nfim:\tHLT\nvar:\tDS 7"		
		sc = AnalisadorLéxico(texto)		
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
				Símbolo("DefiniçãoDeRótulo", "LOC"), Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
				Símbolo("Operador", "ADD"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
				Símbolo("Operador", "STA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
				Símbolo("Operador", "SUB"), Símbolo("#"), Símbolo("Numeral", 100), Símbolo("NovaLinha"),
				Símbolo("Operador", "JN"), Símbolo("Rótulo", "FIM"), Símbolo("NovaLinha"),
				Símbolo("Operador", "JN"), Símbolo("Rótulo", "INI"), Símbolo("NovaLinha"),
				Símbolo("DefiniçãoDeRótulo", "FIM"), Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DS"), Símbolo("Numeral", 7), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
			
	def testeAnalisadorLéxico3(self):
		texto = "\tzero: ADC var\n\tJZ fim\tJMP zero\nvar: D1S 0"		
		sc = AnalisadorLéxico(texto)		
		self.assertRaises(ExceçãoSímboloInválido, sc.iniciaAnálise)
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("DefiniçãoDeRótulo", "ZERO"), Símbolo("Operador", "ADC"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "JZ"), Símbolo("Rótulo", "FIM"), Símbolo("Operador", "JMP"), Símbolo("Rótulo", "ZERO"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR")
			]
		)
		self.assertEqual(falha, "D1S 0")
		
	def testeAnalisadorLéxico4(self):
		texto = "  ;This is a\t*comment*\nvar: ADC var\nJZ var ;This is another comment"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Operador", "ADC"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "JZ"), Símbolo("Rótulo", "VAR"), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxico5(self):
		texto = "LDa var\nadd #UM\nHLT\nvar: DW 10\nUM EQU 1"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "ADD"), Símbolo("#"), Símbolo("Rótulo", "UM"), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 10), Símbolo("NovaLinha"),
			Símbolo("Rótulo", "UM"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 1), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")

	def testeAnalisadorLéxico6(self):
		texto = "LDa var\ntRap 2\nHLT\nvar: dw 10\n"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "TRAP"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 10), Símbolo("NovaLinha"),
			 Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxico7(self):
		texto = "add #2"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "ADD"), Símbolo("#"), Símbolo("Numeral", 2), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")

	def testeAnalisadorLéxico8(self):
		texto = "LDa var\ntRap 2\nHLT\nvar: DB 10, 61\n"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "TRAP"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DB"), Símbolo("Numeral", 10),Símbolo(","),Símbolo("Numeral", 61), Símbolo("NovaLinha"),
			 Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxicoBin(self):
		texto = "add #0b01010111111111"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "ADD"), Símbolo("#"), Símbolo("Numeral", 5631), Símbolo("EOF")
			]
		)
		self.assertEqual(tokens[2].anotações, "0b01010111111111")
		self.assertEqual(falha, "")

		
	def testeAnalisadorLéxicoHex(self):
		texto = "sub 0xF11F"		
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "SUB"), Símbolo("Numeral", 0xF11F), Símbolo("EOF")
			]
		)
		self.assertEqual(tokens[1].anotações, "0xF11F")
		self.assertEqual(falha, "")
		
	def testeALSequência1(self,):
		texto = "LDA #vet\nADD 2\nSTA temp\nLDA @temp\nADD vet\nHLT\ntemp: DW 0\nvet: Dw 32, 17"
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("#"), Símbolo("Rótulo", "VET"), Símbolo("NovaLinha"),
			Símbolo("Operador", "ADD"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
			Símbolo("Operador", "STA"), Símbolo("Rótulo", "TEMP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "LDA"), Símbolo("@"), Símbolo("Rótulo", "TEMP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "ADD"), Símbolo("Rótulo", "VET"), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "TEMP"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VET"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 32), Símbolo(","), Símbolo("Numeral", 17),
			 Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")		
		
	def testeAnalisadorLéxicoString(self,):
		texto = "texto: STR 'Isso é um texto'"
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("DefiniçãoDeRótulo", "TEXTO"), Símbolo("Diretiva", "STR"), Símbolo("CadeiaDeCaracteres", "Isso é um texto"),
			 Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxicoString2(self,):
		texto = 'LDA #4\nTRAP texto\ntexto: STR "Isso é um texto"'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("#"), Símbolo("Numeral", 4), Símbolo("NovaLinha"),
			Símbolo("Operador", "TRAP"), Símbolo("Rótulo", "TEXTO"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "TEXTO"), Símbolo("Diretiva", "STR"), Símbolo("CadeiaDeCaracteres", "Isso é um texto"),
			Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")	
		
	def testeAnalisadorLéxicoLDI(self,):
		texto = 'LDI 4\nHLT'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDI"), Símbolo("Numeral", 4), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxicoInstruçõesSEPeREP(self,):
		texto = 'SEP\nREP\nXGSP\nXGAC'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "SEP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "REP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "XGSP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "XGAC"), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")


	def testeAnalisadorLéxicoDeslocamento(self,):
		texto = 'LDA var+1\nHLT\nvar: DS 2'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("+"), Símbolo("Numeral", 1), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"), 
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DS"), Símbolo("Numeral", 2), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxicoDeslocamento2(self,):
		texto = 'LDA @var+0xA\nHLT'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "LDA"), Símbolo("@"), Símbolo("Rótulo", "VAR"), Símbolo("+"), Símbolo("Numeral", 0xA), Símbolo("NovaLinha"),
			Símbolo("Operador", "HLT"), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")
		
	def testeAnalisadorLéxicoDeslocamento3(self,):
		texto = 'SUB #var+0b10'
		sc = AnalisadorLéxico(texto)
		sc.iniciaAnálise()
		tokens, falha = sc.símbolosProcessados, sc.textoNãoProcessado
		self.assertEqual(tokens, [
			Símbolo("Operador", "SUB"), Símbolo("#"), Símbolo("Rótulo", "VAR"), Símbolo("+"), Símbolo("Numeral", 0b10), Símbolo("EOF")
			]
		)
		self.assertEqual(falha, "")	
		
	def testeSímboloInválido1(self):
		def instanciaSímbolos():
			tokens = [Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "LDA"), Símbolo("Rótulo", "var"), Símbolo("Numeral", 2),				
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido2(self):
		def instanciaSímbolos():
			tokens = [Símbolo("Diretiva", "LDA"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 2),				
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido3(self):
		def instanciaSímbolos():
			tokens = [Símbolo("DefiniçãoDeRótulo", "ORG"), Símbolo("Numeral", 13),
				Símbolo("Operador", "ADD"), Símbolo("Rótulo", "ORG"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"),			
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido4(self):
		def instanciaSímbolos():
			tokens = [Símbolo("DefiniçãoDeRótulo", "TRAP"), Símbolo("Numeral", 13),
				Símbolo("Operador", "ADD"), Símbolo("Rótulo", "TRAP"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"),			
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido5(self):
		def instanciaSímbolos():
			tokens = [Símbolo("Operador", "LDA"), Símbolo("Numeral", "alfa"),
				Símbolo("Operador", "SHL"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"),			
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido6(self):
		def instanciaSímbolos():
			tokens = [Símbolo("Rótulo", "HLT"), Símbolo("NovaLinha"),
				 Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Operador", "ADD"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
	def testeSímboloInválido7(self):
		def instanciaSímbolos():
			tokens = [Símbolo("Operador", "ADD"), Símbolo("Numeral", 13), Símbolo("Rótulo", "HLT"), Símbolo("NovaLinha"),
				 Símbolo("EOF")]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)

	def testeSímboloInválido8(self):
		#Remoção de LDS/STS em favor de XGSP
		def instanciaSímbolos():
			tokens = [
				Símbolo("Operador", "STS"), Símbolo("NovaLinha"),
				Símbolo("Operador", "LDS"),			
				 Símbolo("EOF")
			]
		self.assertRaises(ExceçãoSímboloInválido, instanciaSímbolos)
		
