
import unittest
from SimuS.compilador import Símbolo, AnalisadorSintático
from SimuS.compilador import exceções
from SimuS.simulador import Memória, parâmetros
from SimuS.compilador.analisadorLéxicoBase import Símbolo
from SimuS.compilador.analisadorLéxicoBase import Símbolo

class AnalisadorSintáticoCasoDeTeste(unittest.TestCase):
	def setUp(self):
		self.mem = Memória(tamanhoDoEndereço = parâmetros.tamanhoDoEndereço,
						 tamanhoDaPalavra = parâmetros.tamanhoDaPalavra,
						  ordemDosBytes=parâmetros.endianness)
		self.novaMem = Memória(tamanhoDoEndereço = parâmetros.tamanhoDoEndereço,
						 tamanhoDaPalavra = parâmetros.tamanhoDaPalavra,
						  ordemDosBytes=parâmetros.endianness)
		
	def testeAnalisadorSintático1(self):
		tokens = [
			Símbolo("DefiniçãoDeRótulo", "ZERO"), Símbolo("Operador", "ADC"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "JZ"), Símbolo("Rótulo", "FIM"), Símbolo("NovaLinha"),
			Símbolo("Operador", "JMP"), Símbolo("Rótulo", "ZERO"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "FIM"), Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0xAAee), Símbolo("EOF")
			]
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		
		varRótulo = (10).to_bytes(2, "little")
		fimRótulo = (9).to_bytes(2, "little")
		zeroRótulo = (0).to_bytes(2, "little")

		código = bytearray((0x34, varRótulo[0], varRótulo[1],
						0xa0,fimRótulo[0], fimRótulo[1],
						0x80,zeroRótulo[0], zeroRótulo[1],
						0xFF,
						0xEE, 0xAA))		
		self.novaMem[:len(código)] = código
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		self.assertEqual(len(self.mem.dados), len(self.novaMem.dados))
		
	def testeAnalisadorSintático2(self):
		tokens = [
			Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 104), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "INI"), Símbolo("Operador", "LDA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "STA"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Operador", "SUB"), Símbolo("#"), Símbolo("Numeral", 100), Símbolo("NovaLinha"),
			Símbolo("Operador", "JN"), Símbolo("Rótulo", "FIM"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "FIM"), Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0), Símbolo("EOF")
		]
				
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		
		varRótulo = list((104 + 13).to_bytes(2, "little"))		
		fimRótulo = list((104 + 12).to_bytes(2, "little"))
		
		código = [0x20, varRótulo[0], varRótulo[1],
			0x10, varRótulo[0], varRótulo[1],
			0x3A, 100, 0,
			0x90, fimRótulo[0], fimRótulo[1],
			0xFF,]
		self.novaMem[104:104+len(código)] = código
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		
	def testeAnalisadorSintáticoSequência1(self):
		tokens = [Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VET"), Símbolo("Diretiva", "DW"), Símbolo("Numeral", 32),
			 Símbolo(","), Símbolo("Numeral", 17), Símbolo(","), Símbolo("Numeral", 89),
			 Símbolo("EOF"),
			]
		
		código = [0x20, 0x00, 0x11, 0x00, 0x59, 0x00]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)

	def testeAnalisadorSintáticoSequência2(self):
		tokens = [Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VET"), Símbolo("Diretiva", "DB"), Símbolo("Numeral", 32),
			 Símbolo(","), Símbolo("Numeral", 17), Símbolo(","), Símbolo("Numeral", 89),
			 Símbolo("EOF"),
			]
		
		código = [0x20, 0x11, 0x59]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)
	
	def testeDefiniçãoDeRótuloComQuebraDeLinha(self,):
		tokens = [
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0xFF), Símbolo("NovaLinha"),			
			 Símbolo("EOF"),
			]
		
		código = [0xFF]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)

	def testeRótuloComoOperandoParaDiretiva(self,):
		tokens = [
			Símbolo("DefiniçãoDeRótulo", "VAR_PTR"), Símbolo("Diretiva", "DW"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DB"), Símbolo("Numeral", 0x32), Símbolo("NovaLinha"),
			 Símbolo("EOF"),
			]
		
		código = [0x02, 0x00, 0x32]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)
			
	def testeRótuloComoOperandoParaDiretiva2(self,):
		tokens = [
			Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR"), Símbolo("Diretiva", "DB"), Símbolo("Numeral", 0x32), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "VAR_PTR"), Símbolo("Diretiva", "DW"), Símbolo("Rótulo", "VAR"), Símbolo("NovaLinha"),
			 Símbolo("EOF"),
			]
		
		código = [0xFF,0x32, 0x01, 0x00]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		
		
	def testeAnalisadorSintáticoString(self,):
		tokens = [
			Símbolo("Diretiva", "STR"), Símbolo("CadeiaDeCaracteres", "AZaz19"), Símbolo("NovaLinha"),
			Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0xFF), Símbolo("NovaLinha"),			
			 Símbolo("EOF"),
			]
		
		código = [0x41, 0x5a, 0x61, 0x7a, 0x31, 0x39, 0x00, 0xFF, 0x00]		
		self.novaMem[:len(código)] = código
		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		
	def testeORG1(self):
		tokens = [
				Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
				Símbolo("Operador", "ADD"), Símbolo("Numeral", 54), Símbolo("NovaLinha"),
				Símbolo("Operador", "SHL"), Símbolo("NovaLinha"),
				 Símbolo("Operador", "SHR"), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 6), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]	
		
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoUsoIncorretoDaMemória, asin.iniciaAnálise)
		
	def testeORG2(self):
		tokens = [
				Símbolo("Operador", "ADD"), Símbolo("Numeral", 54), Símbolo("NovaLinha"),
				Símbolo("Operador", "SHL"), Símbolo("NovaLinha"),
				 Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 0), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "DW"), Símbolo("Numeral", 0), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]	
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoUsoIncorretoDaMemória, asin.iniciaAnálise)
		
	def testeORG3(self):
		tokens = [
				Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
				Símbolo("Operador", "ADD"), Símbolo("#"), Símbolo("Numeral", 123), Símbolo("NovaLinha"),
				 Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 5), Símbolo("NovaLinha"),
				Símbolo("Operador", "JMP"), Símbolo("Numeral", 0), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [50, 123, 0 , 128, 0, 0, 255]
		self.novaMem[2:2+len(código)] = código
		self.assertEqual(self.mem.dados, self.novaMem.dados)
			
		
	def testeEQU1(self):
		tokens = [
				Símbolo("Operador", "LDA"), Símbolo("Rótulo", "MAX"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Rótulo", "MAX"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 1223), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x20, 0xc7, 0x04, 0xFF]
		self.mem[:len(código)] = código			
		self.assertEqual(self.mem.dados, asin.memória.dados)
		
	def testeEQU2(self):
		tokens = [
				Símbolo("Operador", "ADD"), Símbolo("Rótulo", "VALUE"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Rótulo", "VALUE"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 18), Símbolo("EOF")
				]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x30, 0x12, 0x00, 0xFF]		
		self.mem[:len(código)] = código			
		self.assertEqual(self.mem.dados, asin.memória.dados)
		
	def testeEQU3(self):
		tokens = [
				Símbolo("Operador", "ADD"), Símbolo("#"), Símbolo("Rótulo", "VALUE"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Rótulo", "VALUE"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 18), Símbolo("EOF")
				]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x32, 0x12, 0xFF]		
		self.mem[:len(código)] = código			
		self.assertEqual(self.mem.dados, asin.memória.dados)

	def testeEND1(self):
		tokens = [
			Símbolo("Operador", "NOP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "NOP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "NOP"), Símbolo("NovaLinha"),
			Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
			Símbolo("Operador", "NOP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "NOP"), Símbolo("NovaLinha"),
				  Símbolo("Diretiva", "END"), Símbolo("Rótulo", "var"), Símbolo("NovaLinha"),
				 Símbolo("EOF")]		
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x00,0x00,0x00, 0xFF,]		
		self.novaMem[:len(código)] = código

		self.assertEqual(asin.end, 3)
		self.assertEqual(self.mem.dados, self.novaMem.dados)

	def testeEND2(self):
		tokens = [
				  Símbolo("Diretiva", "END"), Símbolo("Numeral", 1), Símbolo("NovaLinha"),
				  Símbolo("Diretiva", "END"), Símbolo("Numeral", 2), Símbolo("NovaLinha"),
				 Símbolo("EOF")]		

		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)

	def testeEND3(self):
		tokens = [
				  Símbolo("Diretiva", "END"), Símbolo("Rótulo", "var"), Símbolo("NovaLinha"),
				 Símbolo("EOF")]		

		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoRótuloSemDefinição, asin.iniciaAnálise)
		
	def testeDS(self):
		tokens = [
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"), 
				Símbolo("Diretiva", "DS"), Símbolo("Numeral", 12), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")
				]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.novaMem[0] = 0xFF	
		self.novaMem[13] = 0xFF			
		self.assertIn((1, "DS", 12, 1), asin.endereçosDasAlocações)	
		self.assertEqual(self.novaMem.dados, self.mem.dados)
		
	def testeDW(self):
		tokens = [
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "DW"), Símbolo("Numeral", 12), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")
				]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()	
		self.novaMem[0] = 0xFF		
		self.novaMem[1] = 12	
		self.novaMem[3] = 0xFF
		self.assertIn((1, "DW", self.mem.tamanhoDoEndereço, 1), asin.endereçosDasAlocações)	
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		
	def testeDeslocamento(self):
		tokens = [ Símbolo("Operador", "LDA"), Símbolo("Rótulo", "var"), Símbolo("+"), Símbolo("Numeral", 0b101), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Diretiva", "DS"), Símbolo("Numeral", 10), Símbolo("EOF") ]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x20, 0x04 + 5, 0x00, 0xFF]		
		self.novaMem[:len(código)] = código
		self.assertEqual(self.mem.dados, self.novaMem.dados)		

	def testeREPeSEP(self):
		tokens = [
			Símbolo("Operador", "SEP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "REP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "XGSP"), Símbolo("NovaLinha"),
			Símbolo("Operador", "XGAC"), Símbolo("EOF")
		]
		asin = AnalisadorSintático(tokens, self.mem)
		asin.iniciaAnálise()
		código = [0x04, 0x08, 0x14, 0x18]		
		self.novaMem[:len(código)] = código
		self.assertEqual(self.mem.dados, self.novaMem.dados)
		
	def testeExceçãoOperandoInválido1(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Diretiva","DS"), Símbolo("Numeral", 0), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoOperandoInválido,asin.iniciaAnálise)	
		
	def testeExceçãoOperandoInválido2(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Diretiva","DW"), Símbolo("Numeral", 0xFFFF+1), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoOperandoInválido, asin.iniciaAnálise)

	def testeExceçãoOperandoInválido2(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				Símbolo("Diretiva","DB"), Símbolo("Numeral", 0xFF+1), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoOperandoInválido, asin.iniciaAnálise)
		
	def testeExceçãoValorForaDosLimites(self):
		tokens = [Símbolo("Operador", "SUB"), Símbolo("Numeral", 0xFFFF + 1), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoValorForaDosLimites, asin.iniciaAnálise)
		
	def testeExceçãoValorForaDosLimites2(self):
		tokens = [Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 0xFFFF +1), Símbolo("NovaLinha"),
				 Símbolo("Operador", "ADD"), Símbolo("Rótulo", "var"),  Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoValorForaDosLimites, asin.iniciaAnálise)
		
	def testeSímboloInesperado1(self):
		tokens = [Símbolo("Operador", "ADD"), Símbolo("Numeral", 18), Símbolo("Operador", "ADD"), Símbolo("Numeral", 18), Símbolo("EOF")]
	
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado2(self):
		tokens = [Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("DefiniçãoDeRótulo", "teste"),
				 Símbolo("Operador", "ADD"), Símbolo("Numeral", 123), Símbolo("EOF")]	
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)		
		
	def testeSímboloInesperado3(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("NovaLinha"), 
				Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Operador", "ADD"),
								Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado4(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("NovaLinha"),
				 Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Diretiva", "END"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				 Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado5(self):
		tokens = [Símbolo("Operador", "HLT"), Símbolo("Numeral", 13), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado6(self):
		tokens = [Símbolo("Operador", "SHL"), Símbolo("Numeral", 13), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
			
	def testeSímboloInesperado7(self):
		tokens = [Símbolo("Operador", "JMP"), Símbolo("#"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado8(self):
		tokens = [Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Diretiva", "ORG"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado9(self):
		tokens = [Símbolo("Operador", "ADD"), Símbolo("Operador", "ADD"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado10(self):
		tokens = [Símbolo("Operador", "ADD"), Símbolo("@"), Símbolo("Operador", "ADD"), Símbolo("NovaLinha"),
				Símbolo("Operador", "HLT"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
	
	def testeSímboloInesperado11(self):
		tokens = [Símbolo("Operador", "ADD"), Símbolo("@"), Símbolo("Numeral", 12), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "ORG"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeSímboloInesperado12(self):
		tokens = [Símbolo("Operador", "LDA"), Símbolo("@"), Símbolo("Numeral", 12), Símbolo("NovaLinha"),
				Símbolo("Diretiva", "EQU"), Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoSímboloInesperado, asin.iniciaAnálise)
		
	def testeRótuloInválido1(self):
		tokens = [Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Operador", "ADD"), Símbolo("Numeral", 123), Símbolo("NovaLinha"),
				Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Operador", "ADD"), Símbolo("Numeral", 123),				
				 Símbolo("EOF")]	
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoDefiniçãoDeRótuloRepetida, asin.iniciaAnálise)
	
	def testeRótuloInválido2(self):
		tokens = [Símbolo("DefiniçãoDeRótulo", "var"), Símbolo("Operador", "ADD"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 12),				
				 Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoDefiniçãoDeRótuloRepetida, asin.iniciaAnálise)
		
	def testeRótuloInválido3(self):
		tokens = [Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 13), Símbolo("NovaLinha"),
				Símbolo("Rótulo", "var"), Símbolo("Diretiva", "EQU"), Símbolo("Numeral", 2),				
				 Símbolo("EOF")]
		asin = AnalisadorSintático(tokens, self.mem)
		self.assertRaises(exceções.ExceçãoDefiniçãoDeRótuloRepetida, asin.iniciaAnálise)