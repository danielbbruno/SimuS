
import unittest
from unittest.mock import Mock
from SimuS.simulador import Máquina, Memória
from SimuS.simulador.exceções import *
from SimuS.simulador.parâmetros import endianness, tamanhoDaPalavra, tamanhoDoEndereço

class máquinaModoOitoBitsTeste(unittest.TestCase):
	"""Testes das instruções definidas na classe Máquina, no modo de 8 bits. Também testa as instruções de manipulação do bit de status"""
	def setUp(self):
		memória = Memória(tamanhoDoEndereço = tamanhoDoEndereço, tamanhoDaPalavra = tamanhoDaPalavra, ordemDosBytes=endianness)
		self.máquinaInicial = Máquina(memória, cp=None)
		self.máquinaInicial.modoOitoBits = True

		self.máquina = Máquina(memória, cp=None)
		self.máquina.modoOitoBits = True
		
	def tearDown(self):
		del(self.máquina)
		del(self.máquinaInicial)

	
	def testeNOP(self,):
		#Igual ao modo de 16 bits
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)

	def testeSEP(self,):
		#Igual ao modo de 16 bits
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x04, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.modoOitoBits, True)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeREP(self,):
		#Igual ao modo de 16 bits
		dado = bytearray([0x08, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
	
		self.assertEqual(self.máquina.modoOitoBits, False)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeSTA(self):
		self.máquina.acumulador = 0x0AFA
		dado = bytearray([0x10, 0x04, 0x00])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.memória[3], 0x00)
		self.assertEqual(self.máquina.memória[4], 0xFA)
		self.assertEqual(self.máquina.memória[5], 0x00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		self.assertEqual(self.máquina.modoOitoBits, self.máquinaInicial.modoOitoBits)
	
	def testeSTAind(self):
		self.máquina.acumulador = 0xCF3A
		dado = bytearray([0x11, 0x04, 0x00, 0xFF, 0x06, 0x00])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.memória[5], 0x00)
		self.assertEqual(self.máquina.memória[6], 0x3A)
		self.assertEqual(self.máquina.memória[7], 0x00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeXGSP(self):
		#Igual ao modo de 16 bits
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0xF2FA
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0xAC23
		dado = bytearray([0x14, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
		
	def testeXGAC(self,):
		#Igual ao modo de 16 bits
		self.máquina.acumulador = 0xAC23
		dado = bytearray([0x18, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x23AC)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)


	def testeLDA(self):
		dado = bytearray([0x20, 0x04, 0x00, 0xFF, 0x7A, 0x12])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x7A)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeLDAind(self):
		dado = bytearray([0x21, 0x04, 0x00, 0xFF, 0x06, 0x00, 0x10, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 16)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag				
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeLDAimm(self):
		dado = bytearray([0x22, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xC112
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xC100)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag) #LDA can't change carryFlag
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)


	def testeADD(self):
		dado = bytearray([0x30, 0x04, 0x00, 0xFF, 0xAB, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x435e
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x4309)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADDimm(self):
		dado = bytearray([0x32, 0x5B, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x0A12
		
		self.máquina.passo()

		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.acumulador, 0x0a6d)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADDind(self):
		dado = bytearray([0x31, 0x04, 0x00, 0xFF, 0x06, 0x00, 0xe0, 0xA0])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x018A
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x016a)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeADC(self):
		dado = bytearray([0x34, 0x04, 0x00, 0xFF, 0x16])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x3A
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x51)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeADCind(self):
		dado = bytearray([0x35, 0x04, 0x00, 0xFF, 0x06, 0x00, 0x00, 0xD5])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xEAFF
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xea00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
		
	def testeADCimm(self):
		dado = bytearray([0x36, 0xFF, 0x20, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xDF00
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xDFFF)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSUB(self):
		dado = bytearray([0x38, 0x04, 0x00, 0xFF, 0x0A, 0x0b])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x12
		self.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x8)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)			
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSUBind(self):
		dado = bytearray([0x39, 0x06, 0x00, 0xFF, 0xBB, 0x00, 0x04, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x12AF
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.acumulador, 0x12f4)
		self.assertTrue(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
		
	def testeSUBimm(self):
		dado = bytearray([0x3A, 0x17, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xAC17
		self.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xAC00)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeSBC(self):
		dado = bytearray([0x3c, 0x04, 0x00, 0xFF, 0x0A, 0xb])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x0a17
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x0a0b)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeSBCimm(self):
		dado = bytearray([0x3e, 0x03, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xab02
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xabff)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)

	def testeOR(self):
		dado = bytearray([0x40, 0x04, 0x00, 0xFF, 0x1B, 0x0f])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x100C
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x101f)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeXORimm(self):
		dado = bytearray([0x46, 0x2e, 0x00, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xA912
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xa93c)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeANDimm(self):
		dado = bytearray([0x52, 0x0F, 0x01])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x03F0
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x0300)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 3)
	
	def testeNOT(self):
		dado = bytearray([0x60, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x015D
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x01A2)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)	
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)

	def testeSHL(self):
		dado = bytearray([0x70, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xE801
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xe802)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSHL2(self):
		dado = bytearray([0x70, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x4180
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x4100)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertTrue(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSHR(self):
		dado = bytearray([0x74, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0xAE03
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0xAE01)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSRA(self):
		dado = bytearray([0x78, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x80d2
		self.máquina.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x80e9)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertFalse(self.máquina.carryFlag)
		self.assertTrue(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testeSRA2(self):
		dado = bytearray([0x78, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = 0x1353
		self.máquina.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x1329)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertTrue(self.máquina.carryFlag)
		self.assertFalse(self.máquina.negativeFlag)
		self.assertFalse(self.máquina.zeroFlag)
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)


	def testeJMPind(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x81, 0x03, 0x00, 0xAE, 0x01])
		self.máquina.memória[:len(dado)] = dado
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x01AE)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJN(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x90, 0x08, 0x00])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True		
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0008)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJN2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x90, 0xAA, 0xCC])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJP(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False				
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJP2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True				
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0003)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
			
	def testeJP3(self):
		#Igual ao modo 16 bits
		dado = bytearray([0x94, 0x08, 0x0A])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False			
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0A08)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJZ(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xA0, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = True			
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJZ2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xA0, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0xAAAB, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJNZ(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xA4, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = True		
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJNZ2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xA4, 0xAB, 0xAA])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.zeroFlag = self.máquinaInicial.zeroFlag = False	
		self.máquina.negativeFlag = self.máquinaInicial.negativeFlag = False		
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0xAAAB, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeJC(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xb0, 0x00, 0x10 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x1000, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJC2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xb0, 0x08, 0x00 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJNC(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xb4, 0x00, 0x10 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = True
		
		self.máquina.passo()
		
		self.assertEqual(0x0003, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
		
	def testeJNC2(self):
		#Igual ao modo 16 bits
		dado = bytearray([0xb4, 0x08, 0x00 , 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.carryFlag = self.máquinaInicial.carryFlag = False
		
		self.máquina.passo()
		
		self.assertEqual(0x0008, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)

	def testePUSH(self):
		dado = bytearray([0xE0, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x0312
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0x10FF
		
		self.máquina.passo()
		
		self.assertEqual(
			int.from_bytes(
				self.máquina.memória[
					self.máquina.apontadorDePilha:
					self.máquina.apontadorDePilha + 1
				],
				self.máquina.memória.ordemDosBytes
			)
			, 0x12 
		)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha - 1)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)
	
	def testePOP(self):
		dado = bytearray([0xE4, 0xFF])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.acumulador = self.máquinaInicial.acumulador = 0x112a
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0x0aFF
		self.máquina.memória[self.máquina.apontadorDePilha] = 0xe5
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.acumulador, 0x11e5)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha + 1)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, True)
		self.assertEqual(self.máquina.zeroFlag, False)		
		self.assertEqual(self.máquina.apontadorDeInstrução, self.máquinaInicial.apontadorDeInstrução + 1)

	def testeJSR(self):
		#Mesmo funcionamento do modo de 16 bits
		dado = bytearray([0xd0, 0xAC, 0x10])
		end = 0x31FA
		self.máquina.memória[end:end + len(dado)] = dado
		self.máquina.apontadorDePilha = self.máquinaInicial.apontadorDePilha = 0xFBFA
		self.máquina.apontadorDeInstrução = end
		
		self.máquina.passo()
		
		self.assertEqual(0x10AC, self.máquina.apontadorDeInstrução)
		self.assertEqual(self.máquina.acumulador, self.máquinaInicial.acumulador)
		self.assertEqual(self.máquina.apontadorDePilha, self.máquinaInicial.apontadorDePilha - 2)
		self.assertEqual(int.from_bytes(
				self.máquina.memória[
					self.máquina.apontadorDePilha:
					self.máquina.apontadorDePilha + tamanhoDaPalavra
				],
				self.máquina.memória.ordemDosBytes
				),
			len(dado) +end)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)
	
	def testeRET(self):
		#Mesmo funcionamento do modo de 16 bits
		self.máquina.apontadorDePilha = 0xFFAC
		dado = bytearray([0xd8])
		self.máquina.memória[:len(dado)] = dado
		self.máquina.memória[self.máquina.apontadorDePilha: self.máquina.apontadorDePilha + tamanhoDaPalavra] = bytearray((0x40, 0x00))
		
		self.máquina.passo()
		
		self.assertEqual(self.máquina.apontadorDePilha, 0xFFAC + tamanhoDoEndereço )
		self.assertEqual(self.máquina.apontadorDeInstrução, 0x0040)
		self.assertEqual(self.máquina.carryFlag, self.máquinaInicial.carryFlag)
		self.assertEqual(self.máquina.negativeFlag, self.máquinaInicial.negativeFlag)
		self.assertEqual(self.máquina.zeroFlag, self.máquinaInicial.zeroFlag)