
import unittest
from SimuS.simulador import Memória
from SimuS.simulador.parâmetros import endianness, tamanhoDaPalavra, tamanhoDoEndereço

class MemóriaCasoDeTeste(unittest.TestCase):
	def setUp(self):
		self.memória = Memória(tamanhoDoEndereço = tamanhoDoEndereço, tamanhoDaPalavra = tamanhoDaPalavra, ordemDosBytes=endianness)
		
	def testaEscritaNoValorMáximoDeMemória(self):
		dado = bytearray([0x10, 0x04])
		endereçoMáximo = self.memória.endereçoMáximoDeMemória
		tamanhoAntigo = len(self.memória.dados)
		self.memória[endereçoMáximo:endereçoMáximo + len(dado)] = dado
		tamanhoNovo = len(self.memória.dados)
		self.assertEqual(tamanhoAntigo, tamanhoNovo)
		self.assertEqual(self.memória[0], 0x04)
		self.assertEqual(self.memória[-1], 0x10)
		
		
		