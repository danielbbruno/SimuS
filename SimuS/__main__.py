#!/usr/bin/env python3.5

from SimuS.controlador.classePrincipal import ClassePrincipal
import traceback, sys

def main(args=None):
	try:
		m = ClassePrincipal()
		m.inicia()
	except Exception as e:
		print(str(type(e))+ ": " + str(e))
		print('-'*60)
		traceback.print_exc(file=sys.stdout)
		print('-'*60)

if __name__ == "__main__":
	main()
