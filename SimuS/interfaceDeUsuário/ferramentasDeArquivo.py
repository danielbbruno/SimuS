
from PyQt5.QtWidgets import QMessageBox, QFileDialog
class FerramentasDeArquivo:
    def __init__(self):
        self.textoInicial = ""
        self.localizaçãoDoArquivo = ""
        self.actionSair.triggered.connect(self.close)
        self.actionNovo_arquivo.triggered.connect(self.novoArquivo)
        self.actionSalvar.triggered.connect(self.salvarArquivo)
        self.actionSalvarComo.triggered.connect(self.salvarArquivoComo)
        self.actionAbrirArquivo.triggered.connect(self.abrirArquivo)
        
        self.actionCopiar.triggered.connect(self.editorDeCodigo.copy)
        self.actionCortar.triggered.connect(self.editorDeCodigo.cut)
        self.actionColar.triggered.connect(self.editorDeCodigo.paste)
        self.actionDesfazer.triggered.connect(self.editorDeCodigo.undo)
        self.actionRefazer.triggered.connect(self.editorDeCodigo.redo)
        
    def closeEvent(self, ev):
        if not self.arquivoModificado():
            ev.accept()
        else:
            resposta = self.confirmaçãoDeSaída()
            if resposta == QMessageBox.Yes:
                ev.accept()
            else:
                ev.ignore()
                
    def confirmaçãoDeSaída(self):
        mensagemDeSaída = "O arquivo aberto foi modificado, mas não foi salvo. Deseja sair mesmo assim?"
        resposta = QMessageBox.question(self, 'Confirmação de saída', 
                         mensagemDeSaída, QMessageBox.Yes, QMessageBox.No)
        return resposta
    
    def arquivoModificado(self):
        #Este método não é uma property do python3 por causa da incompatibilidade com o PyQt
        return self.editorDeCodigo.toPlainText() != self.textoInicial
    
    def novoArquivo(self):
        if not self.arquivoModificado():
            self.editorDeCodigo.document().setPlainText("")
            self.localizaçãoDoArquivo = ""
        else:                
            resposta = self.confirmaçãoDeSaída()
            if resposta == QMessageBox.Yes:
                self.editorDeCodigo.document().setPlainText("")
                self.localizaçãoDoArquivo = ""
                
    def salvarArquivoComo(self):
        nomeDoArquivo = QFileDialog.getSaveFileName(filter="Arquivos de Texto (*.asm *.txt)")[0]
             
        if nomeDoArquivo:
            texto = self.editorDeCodigo.toPlainText()
            with open(nomeDoArquivo, "w") as arquivo:
                arquivo.write(texto)
            self.textoInicial = texto            
            self.localizaçãoDoArquivo = nomeDoArquivo
        
    def salvarArquivo(self):
        if self.localizaçãoDoArquivo:
            texto = self.editorDeCodigo.toPlainText()
            with open(self.localizaçãoDoArquivo, "w") as arquivo:
                arquivo.write(texto)            
            self.textoInicial = texto
        
        else:
            self.salvarArquivoComo()
    
    def abrirArquivo(self):
        nomeDoArquivo = QFileDialog.getOpenFileName(filter="Arquivos de Texto (*.asm *.txt)")
        nomeDoArquivo = nomeDoArquivo[0]
        
        if nomeDoArquivo:
            with open(nomeDoArquivo, "r") as arquivo:
                self.textoInicial = arquivo.read()
            self.editorDeCodigo.document().setPlainText(self.textoInicial)

        self.localizaçãoDoArquivo = nomeDoArquivo
        
    def recortar(self):
        pass
    
    def colar(self):
        pass
    
    def copiar(self):
        pass
    
    def refazer(self):
        pass
    
    def desfazer(self):
        pass
    
    