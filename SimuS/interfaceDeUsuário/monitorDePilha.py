
from SimuS.simulador import Memória
from PyQt5 import QtWidgets, QtCore, QtGui
from SimuS.simulador import parâmetros
from SimuS.interfaceDeUsuário.monitorDeDados import MonitorDeDados

class MonitorDaPilha():
	acompanhaApontadorDePilha = True
	def iniciaMonitorDaPilha(self, colunas=16):
		self.colunasNoMonitorDePilha = colunas
		self.modeloDeMemória = ModeloDePilha(cp=self.cp, colunas=colunas, parent=self)
		re = ItemPilha(cp=self.cp, modelo=self.modeloDeMemória, colunas=colunas)
		self.tabelaPilha.setModel(self.modeloDeMemória)
		self.tabelaPilha.setItemDelegate(re)
		self.tabelaPilha.verticalHeader().setDefaultSectionSize(35)
		self.tabelaPilha.horizontalHeader().setSectionResizeMode( QtWidgets.QHeaderView.Stretch)
		self.tabelaPilha.show()
		
	def atualizaMonitorDaPilha(self, inicio, fim=None):
		índiceInicial = self.modeloDeMemória.index(inicio // self.colunasNoMonitorDePilha, 0)
		if fim is None:
			fim = inicio + 1		
		índiceFim = self.modeloDeMemória.index(fim // self.colunasNoMonitorDePilha, 
												self.colunasNoMonitorDePilha)
		self.tabelaPilha.dataChanged(índiceInicial, índiceFim, [])
		
		apPilha = (self.cp.cSim.máquina.apontadorDePilha) & 0xFFFF
		índiceApontadorDePilha = self.modeloDeMemória.index(
			apPilha // self.colunasNoMonitorDePilha,			
			apPilha % self.colunasNoMonitorDePilha,
		)
		self.tabelaPilha.scrollTo(índiceApontadorDePilha)
		
class ModeloDePilha(QtCore.QAbstractTableModel):
	def __init__(self, cp, colunas, parent=None, *args): 
		super(QtCore.QAbstractTableModel, self).__init__()
		self.cp = cp
		self.colunas = colunas
	
	def headerData(self, section, orientation, role):
		if role == QtCore.Qt.DisplayRole:
			if orientation == QtCore.Qt.Horizontal:
				return QtCore.QVariant(hex(section)[2:].upper().zfill(1))
			elif orientation == QtCore.Qt.Vertical:        		
				return QtCore.QVariant(hex(section)[2:].upper().zfill(3))
		return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)
	
	def flags (self, índice):
		return QtCore.Qt.ItemIsEnabled
		
	def rowCount(self, parent=QtCore.QModelIndex()):
		return int((self.cp.cSim.memória.endereçoMáximoDeMemória + 1)/self.colunas)

	def columnCount(self, parent=QtCore.QModelIndex()):
		return self.colunas

	def data(self, index, role=QtCore.Qt.DisplayRole):
		if not index.isValid():
			return QtCore.QVariant()
		
		índice = index.row()*self.colunas + index.column()
		
		if role == QtCore.Qt.ToolTipRole:
			return "0x" + hex(índice)[2:].upper().zfill(4)
		else:
			if role == QtCore.Qt.DisplayRole:
				return QtCore.QVariant(str(format(self.cp.cSim.memória[índice], "02X")))
			elif role == QtCore.Qt.UserRole:
				return self.tabelaDeTiposNaMemória[índice]
				
	def atualizaOrigemDosDados(self, mem):
		self.cp.cSim.memória = mem
		
class ItemPilha(QtWidgets.QItemDelegate):
	valorMáximoDaPalavra = 0xFF
	corDoApontadorDeInstrução = MonitorDeDados.corDoApontadorDeInstrução
	corDoApontadorDePilha = QtGui.QColor(255, 170, 210)
	
	def __init__(self, cp, modelo, colunas):
		QtWidgets.QItemDelegate.__init__(self)
		self.colunas = colunas
		self.posiçãoApontadorDeInstrução = lambda: modelo.index(cp.cSim.máquina.apontadorDeInstrução // self.colunas,
											 cp.cSim.máquina.apontadorDeInstrução % self.colunas)
		
		self.posiçãoApontadorDePilha = lambda: modelo.index(cp.cSim.máquina.apontadorDePilha// self.colunas,
											 cp.cSim.máquina.apontadorDePilha % self.colunas)
		
	
	def paint(self, painter, option, index):
		r = option.rect
		
		fonte = QtGui.QFont( "Lucida Grande", 12 )

		título = str(index.data(QtCore.Qt.DisplayRole))
		
		if index == self.posiçãoApontadorDePilha():						
			painter.fillRect(r, self.corDoApontadorDePilha)		
		elif index == self.posiçãoApontadorDeInstrução():			
			painter.fillRect(r, self.corDoApontadorDeInstrução)
		else:
			pass
						
		painter.setPen(QtGui.QColor(0,0,0))
			
		painter.setFont(fonte)
		painter.drawText(r.left(), r.top(), r.width(), r.height(), QtCore.Qt.AlignCenter, título)
	
	
	