
import re, pkg_resources

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import pyqtSlot, QUrl
import html

class Console:
	máximoDeMensagens = 100
	mensagens = []
	mensagensAntigas = []
	textoNoBuffer = ""  #Buffer de leitura do console - as coisas escritas nas linhas são adicionados à este buffer
	textoEmEscrita = "" #Buffer de escrita no console ainda sem quebra de linha
	limparAvisosAoMontar = False
	def __init__(self):
		self.entradaDeTexto.returnPressed.connect(self.enterEntradaDeTexto)
		caminho = pkg_resources.resource_filename('SimuS', 'interfaceDeUsuário/prefixo.html')
		with open(caminho) as arq:
			prefixoHtml = arq.read()
		prefixoHtmlSemTabs = "".join(prefixoHtml.split("\t"))
		self.prefixoHtml = "".join(prefixoHtmlSemTabs.split("\n"))
		self.areaDeAvisos.anchorClicked.connect(self.pulaParaLinha)
		
	def escondeAvisosAntigos(self):
		self.areaDeAvisos.clear()
		
	def arquivaMensagens(self):
		def removeLinks(html):
			regex = re.compile('<\/?[a].*?>')
			resultado = re.sub(regex, '', html)
			return resultado
		self.mensagensAntigas.extend([removeLinks(msg) for msg in self.mensagens])
		self.mensagensAntigas = self.mensagensAntigas[:self.máximoDeMensagens]
		self.mensagens = []
		
	def escreveAviso(self, texto):
		msg = "<div class='sistema'>" + str(texto) + "</div>"		
		self.atualizaConsole(msg)
		
	def geraMensagemDoTextoEmEscrita(self,):
		if self.textoEmEscrita:
			return "<div class='programa'>" + html.escape(str(self.textoEmEscrita), True) + "</div>"
		return ""
		
	def atualizaConsole(self, texto):
		if texto is not None:
			self.mensagens.append(texto)
		if self.limparAvisosAoMontar:
			self.mensagensAntigas = []
			
		elif len(self.mensagensAntigas)> self.máximoDeMensagens - len(self.mensagens):
			self.mensagensAntigas = self.mensagensAntigas[:self.máximoDeMensagens - len(self.mensagens)]
			self.mensagens = self.mensagens[:self.máximoDeMensagens]

		textoCompleto = "<body><div class='antigos'>"
		textoCompleto += "".join(self.mensagensAntigas)		
		textoCompleto += "</div><div class='atuais'>"
		textoCompleto += "".join(self.mensagens)
		textoCompleto += self.geraMensagemDoTextoEmEscrita()
		textoCompleto += "</div></body></html>"		
		
		html = self.prefixoHtml + textoCompleto

		#Move até o fim da janela
		self.areaDeAvisos.setHtml(html)
		barra = self.areaDeAvisos.verticalScrollBar()
		barra.setValue(barra.maximum())
		
	def consomeBuffer(self, cadeiaDeCaracteres):
		quebrasDeLinha = self.textoNoBuffer.split("\n")
		
		for i, texto in enumerate(quebrasDeLinha):
			if texto:
				if cadeiaDeCaracteres:	
					saída = texto
					self.textoNoBuffer = "\n".join(quebrasDeLinha[i+1:])
					return saída
				else:
					saída = texto[0]
					self.textoNoBuffer = texto[1:] + "\n".join(quebrasDeLinha[i+1:])
					return saída
		
	def escreveNoConsole(self, texto): #Mensagens de erro, alertas, etc
		if len(texto) <= 1:
			if texto == "\n" or texto == "\0" or not texto:
				texto = self.textoEmEscrita
				self.textoEmEscrita = ""
			else:
				self.textoEmEscrita += texto
				self.atualizaConsole(None)
				return
		else:
			texto = self.textoEmEscrita + texto
			self.textoEmEscrita = ""		
		texto = html.escape(texto, quote=True)
		msg = "<div class='programa'>" + str(texto) + "</div>"
		self.atualizaConsole(msg)
		
	def ecoDoUsuário(self, texto):
		texto = html.escape(texto, quote=True)
		msg = "<div class='usuario'>" + ">>> " + str(texto) + "</div>"
		self.atualizaConsole(msg)
		
	def esvaziaBuffer(self):
		self.textoNoBuffer = ""
		self.textoEmEscrita = ""
		
	@pyqtSlot()
	def enterEntradaDeTexto(self):
		texto = self.entradaDeTexto.text()
		self.textoNoBuffer += texto
		self.entradaDeTexto.setText("")
		self.ecoDoUsuário(texto)
	
	@pyqtSlot(QUrl)
	def pulaParaLinha(self, url):
		numLinha = int(url.toDisplayString())
		bloco = self.editorDeCodigo.document().findBlockByLineNumber(numLinha)
		novoCursor = QtGui.QTextCursor(bloco)
		self.editorDeCodigo.setTextCursor(novoCursor)
			
