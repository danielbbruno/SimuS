
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtCore
from SimuS.interfaceDeUsuário.designer.ui_JanelaPrincipal import Ui_JanelaPrincipal
from SimuS.interfaceDeUsuário.monitorDeDados import MonitorDeDados
from SimuS.interfaceDeUsuário.destaqueDeSintaxe import DestaqueDeSintaxe
from SimuS.interfaceDeUsuário.console import Console
from SimuS.interfaceDeUsuário.editorDeTexto import EditorDeTexto, WidgetEditorDeTexto
from SimuS.interfaceDeUsuário.widgetEstadoDaMáquina import WidgetEstadoDaMáquina
from SimuS.interfaceDeUsuário.widgetControleDeExecução import WidgetControleDeExecução
from SimuS.interfaceDeUsuário.ferramentasDeArquivo import FerramentasDeArquivo
from SimuS.interfaceDeUsuário.widgetConfigurações import WidgetConfigurações
from SimuS.interfaceDeUsuário.monitorDePilha import MonitorDaPilha

class JanelaPrincipal(QMainWindow, Ui_JanelaPrincipal, MonitorDeDados, MonitorDaPilha, Console, EditorDeTexto, FerramentasDeArquivo):
	def __init__(self, cp, áreaDeTransferência,elementoPai=None):
		QMainWindow.__init__(self, elementoPai)
		
		self.cp = cp

		self.setupUi(self)
		
		#Adiciona editor de texto
		self.editorDeCodigo = WidgetEditorDeTexto(self.tabTexto)
		self.editorDeCodigo.setObjectName("editorDeCodigo")
		self.verticalLayout_5.addWidget(self.editorDeCodigo)
		
		MonitorDeDados.__init__(self)
		Console.__init__(self)		
		FerramentasDeArquivo.__init__(self)
		
		self.áreaDeTransferência = áreaDeTransferência
		
		#Inicializa destacador de sintaxe
		self.destacador = DestaqueDeSintaxe(self, self.editorDeCodigo.document())
		
		#Adiciona um menu de contexto para inserir breakpoints
		self.tabelaInstrucoes.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.tabelaInstrucoes.customContextMenuRequested.connect(self.exibeMenuDeBreakpoints)
		
		#Troca para a aba do editorDeTexto
		self.areaSuperior.setCurrentWidget(self.tabTexto)
		
		#Adiciona as widgets externas
		self.widgetEstadoDaMáquina = WidgetEstadoDaMáquina()
		self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.widgetEstadoDaMáquina)
		self.widgetControleDeExecução = WidgetControleDeExecução(cp, self)
		self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.widgetControleDeExecução)
			
		self.widgetConfig = WidgetConfigurações(cp, self)
		self.actionPreferencias.triggered.connect(lambda: self.widgetConfig.show())
		
		#Corrige valor inicial das actions "marcáveis"
		self.acaoEscondeAvisos.toggle()
		self.actionAreaInstrucoes.toggle()
		self.actionAreaPilha.toggle()
		self.actionAreaDados.toggle()
		self.actionEstadoDaMaquina.toggle()
		self.actionControleDeExecucao.toggle()
		
		#Define ações de abrir/fechar widgets 
		self.acaoEscondeAvisos.toggled.connect(self.ativaWidgetDeAvisos)
		self.actionAreaInstrucoes.toggled.connect(geraAtivaçãoDeWidget(self.widgetInstrucoes))
		self.actionAreaPilha.toggled.connect(geraAtivaçãoDeWidget(self.widgetPilha))
		self.actionAreaPilha.toggle() #Inicia com a área de pilha fechada
		self.actionAreaDados.toggled.connect(geraAtivaçãoDeWidget(self.widgetDados))
		self.actionEstadoDaMaquina.toggled.connect(geraAtivaçãoDeWidget(self.widgetEstadoDaMáquina))
		self.actionControleDeExecucao.toggled.connect(geraAtivaçãoDeWidget(self.widgetControleDeExecução))
		
		#Sobreescreve closeEvent para atualizar valor da action associada
		self.widgetInstrucoes.closeEvent = geraCloseEvent(self.actionAreaInstrucoes)
		self.widgetPilha.closeEvent = geraCloseEvent(self.actionAreaPilha)
		self.widgetDados.closeEvent = geraCloseEvent(self.actionAreaDados)
		self.widgetEstadoDaMáquina.closeEvent = geraCloseEvent(self.actionEstadoDaMaquina)
		self.widgetControleDeExecução.closeEvent = geraCloseEvent(self.actionControleDeExecucao)
		
	def ativaWidgetDeAvisos(self):
		if self.areaDeAvisos.isHidden():
			self.area.show()
			self.areaDeAvisos.show()
			self.entradaDeTexto.show()
		else:
			self.area.hide()
			self.areaDeAvisos.hide()
			self.entradaDeTexto.hide()
			
	def ativaAreaDeInstrucoes(self):
		if self.widgetInstrucoes.isHidden():
			self.widgetInstrucoes.show()
		else:
			self.widgetInstrucoes.hide()
			
	def ativaAreaDeDados(self):
		if self.widgetInstrucoes.isHidden():
			self.widgetInstrucoes.show()
		else:
			self.widgetInstrucoes.hide()
	
			
def geraAtivaçãoDeWidget(widget,):
	def ativaWidget():
		if widget.isHidden():
			widget.show()
		else:
			widget.hide()
	return ativaWidget
			
def geraCloseEvent(action):
	def closeEvent(event):
		action.toggle()
		event.accept()
	return closeEvent