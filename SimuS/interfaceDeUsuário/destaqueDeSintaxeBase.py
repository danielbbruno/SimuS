
from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QSyntaxHighlighter

class DestaqueDeSintaxeBase(QSyntaxHighlighter):
	def __init__(self, elementoPai, documento):
		QSyntaxHighlighter.__init__(self, documento)
		self.parent = elementoPai
		
	def highlightBlock( self, texto ):
		for regra in self.regras:
			expressão = QRegExp(regra.padrão)
			índice = expressão.indexIn( texto )
			while índice >= 0:
				tamanho = expressão.matchedLength()
				self.setFormat( índice, tamanho, regra.formatação )
				índice = expressão.indexIn(texto, índice + tamanho)
		self.setCurrentBlockState( 0 )
		
class Regra:
	def __init__(self, padrão, formatação):
		self.padrão = padrão
		self.formatação = formatação