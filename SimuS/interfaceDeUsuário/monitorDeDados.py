from PyQt5.QtCore import pyqtSlot
from PyQt5 import QtWidgets, QtGui, QtCore

class MonitorDeDados():
	corDeDefeito = QtGui.QColor(211, 119, 21)
	corDoApontadorDeInstrução = QtGui.QColor(255,255,200)
	corDeFundoPadrão = QtGui.QColor(255,255,255)
	corDeAtualização = QtGui.QColor(220,220,220)
	corDeFundoBreakpoint = QtGui.QColor(160,230,200)
	
	def __init__(self):
		self.acompanhaPosição = False
	
	@pyqtSlot(QtCore.QPoint)		
	def exibeMenuDeBreakpoints(self, ponto):
		glob = self.tabelaInstrucoes.mapToGlobal(ponto)
		menu = QtWidgets.QMenu()
		
		açãoBkpt = QtWidgets.QAction()
		açãoBkpt.setText("Ativa/Desativa breakpoint")
		açãoBkpt.triggered.connect(self.cp.cSim.ativaBreakpoint)
		
		itens = self.tabelaInstrucoes.selectedItems()
		if len(itens) == 4:
			if itens[0].data(QtCore.Qt.UserRole) != "None":
				açãoBkpt.setEnabled(True)
			else:				
				açãoBkpt.setEnabled(False)			
		else:
			açãoBkpt.setEnabled(False)
		menu.addAction(açãoBkpt)		
		menu.exec(glob)
	
	def iniciaMonitorDeDados(self):
		memória = self.cp.cSim.memória
		self.endereçosDosOperadores = None
		self.endereçoDasAlocações = None
		
		posiçãoDosDados = 3
		posiçãoDoCódigo = 2
		posiçãoDoRótulo = 1
		posiçãoDoEndereço = 0
		
		self.últimoApontadorDeInstrução = 0
				
		self.itensDeInstruções = {}
		self.itensDeDados = {}
		
		self.instruçõesAlteradas = []
		self.últimoDadoAlterado = []
		self.dadosAlterados = []
				
		self.tabelaInstrucoes.horizontalHeader().setSectionResizeMode(posiçãoDoEndereço, QtWidgets.QHeaderView.ResizeToContents)		
		self.tabelaInstrucoes.horizontalHeader().setSectionResizeMode(posiçãoDosDados, QtWidgets.QHeaderView.Stretch)
		self.tabelaInstrucoes.horizontalHeader().setSectionResizeMode(posiçãoDoCódigo, QtWidgets.QHeaderView.Stretch)
		self.tabelaInstrucoes.verticalHeader().hide()
		self.tabelaInstrucoes.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		self.tabelaInstrucoes.verticalHeader().setDefaultSectionSize(20)
		
		self.tabelaDados.horizontalHeader().setSectionResizeMode(posiçãoDoEndereço, QtWidgets.QHeaderView.ResizeToContents)		
		self.tabelaDados.horizontalHeader().setSectionResizeMode(posiçãoDosDados, QtWidgets.QHeaderView.Stretch)
		self.tabelaDados.horizontalHeader().setSectionResizeMode(posiçãoDoCódigo, QtWidgets.QHeaderView.Stretch)
		self.tabelaDados.verticalHeader().hide()
		self.tabelaDados.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		self.tabelaDados.verticalHeader().setDefaultSectionSize(20)
		
	def atualizaMonitorDeDados(self, inicio, fim):
		if fim < inicio:
			self.atualizaMonitorDeDados(0, fim)
			self.atualizaMonitorDeDados(inicio, self.cp.cSim.memória.endereçoMáximoDeMemória)
		else:
			#Define cor da edição de instruções
			conjuntoAtualizado = set([inicio + j for j in range(fim - inicio)])
			if self.endereçosDosOperadores is not None:
				for end, lin, tamOper in self.endereçosDosOperadores:
					conjuntoInstrução = set([(end + i) & self.cp.cSim.memória.endereçoMáximoDeMemória for i in range(tamOper+1) ])
					if not conjuntoAtualizado.isdisjoint(conjuntoInstrução):
						self.instruçõesAlteradas.append(end)
						linha = self.itensDeInstruções[end]
						for j in range(4):
							item = self.tabelaInstrucoes.item(linha, j)
							item.setBackground(self.corDeDefeito)		
			
			#Atualiza cor das alterações nos dados
			if self.endereçoDasAlocações is not None:
				for end, pseudoOp, tam, lin in self.endereçoDasAlocações:
					conjuntoAlocações = set([(end+i) & self.cp.cSim.memória.endereçoMáximoDeMemória for i in range(tam)])
					linha = self.itensDeDados[end]
					if not conjuntoAtualizado.isdisjoint(conjuntoAlocações):
						cod = ""
						for i in self.cp.cSim.memória[end:end+tam]:
							cod += hex(i)[2:].upper().zfill(2) + " "
						cod = cod[:-1]
						itemCod = QtWidgets.QTableWidgetItem(cod)
						self.tabelaDados.setItem(linha, 3, itemCod)
						self.últimoDadoAlterado.append(end)
						for j in range(4):
							item = self.tabelaDados.item(linha, j)
							item.setBackground(self.corDeAtualização)
						#if self.acompanhaPosição:
						self.tabelaDados.scrollToItem(item)
						
	def desmarcaBreakpoint(self, linha, end):
		item = self.tabelaInstrucoes.item(linha, 0)
		if self.cp.cSim.máquina.apontadorDeInstrução == end:
			item.setBackground(self.corDoApontadorDeInstrução)
		else:
			item.setBackground(self.corDeFundoPadrão)
		
		self.tabelaInstrucoes.clearSelection()
		self.tabelaDados.clearSelection()
		
	def atualizaApontadorDePrograma(self, primeiroValor=False):
		if not primeiroValor:
			if self.últimoApontadorDeInstrução in self.itensDeInstruções.keys():
				linhaAntiga = self.itensDeInstruções[self.últimoApontadorDeInstrução]
				item = self.tabelaInstrucoes.item(linhaAntiga, 0)
				for j in range(0,4):
					item = self.tabelaInstrucoes.item(linhaAntiga, j)
					item.setBackground(self.corDeFundoPadrão)
		
		self.últimoApontadorDeInstrução = self.cp.cSim.máquina.apontadorDeInstrução
		
		if self.últimoApontadorDeInstrução in self.itensDeInstruções.keys():
			linha = self.itensDeInstruções[self.últimoApontadorDeInstrução]
			for j in range(4):
				item = self.tabelaInstrucoes.item(linha, j)
				item.setBackground(self.corDoApontadorDeInstrução)
			if self.acompanhaPosição:
				self.tabelaInstrucoes.scrollToItem(item)
		
		#Apaga destaque de dados alterados
		for end in self.dadosAlterados:
			linha = self.itensDeDados[end]
			for i in range(4):
				item = self.tabelaDados.item(linha, i)
				item.setBackground(self.corDeFundoPadrão)
		self.dadosAlterados = self.últimoDadoAlterado		
		self.últimoDadoAlterado = []
		
		for end in self.cp.cSim.breakpoints:
			linha = self.itensDeInstruções[end]
			item = self.tabelaInstrucoes.item(linha, 0)
			item.setBackground(self.corDeFundoBreakpoint)
		
		self.tabelaInstrucoes.clearSelection()
		self.tabelaDados.clearSelection()
		
	def preencheMonitorDeDados(self,):		
		self.tabelaInstrucoes.setRowCount(0)
		self.tabelaDados.setRowCount(0)
		
		self.itensDeInstruções = {}
		self.itensDeDados = {}
		
		self.instruçõesAlteradas = []
		self.últimoDadoAlterado = []
		self.dadosAlterados = []
		
		if self.endereçosDosOperadores is not None:
			for i, (end, lin, tamOper) in enumerate(self.endereçosDosOperadores):
				numLinhas = self.tabelaInstrucoes.rowCount()
				self.tabelaInstrucoes.insertRow(numLinhas)
				
				itemEnd = QtWidgets.QTableWidgetItem("0x" + hex(end)[2:].upper().zfill(4))
				self.tabelaInstrucoes.setItem(numLinhas, 0, itemEnd)
				self.itensDeInstruções[end] = i
				
				cod = ""
				for i in self.cp.cSim.memória[end:end + tamOper +1]:
					cod += hex(i)[2:].upper().zfill(2) + " "
				cod = cod[:-1]
				itemCod = QtWidgets.QTableWidgetItem(cod)
				self.tabelaInstrucoes.setItem(numLinhas, 3, itemCod)
				
				texto = self.cp.cCom.textoSimplificado.split("\n")[lin].lstrip()
				itemTexto = QtWidgets.QTableWidgetItem(texto)
				self.tabelaInstrucoes.setItem(numLinhas, 2, itemTexto)
				
				texto = ""
				for chave, valor in self.definiçõesDeRótulos.items():
					if valor[0] == end:
						texto = chave
				itemTexto = QtWidgets.QTableWidgetItem(texto)		
				self.tabelaInstrucoes.setItem(numLinhas, 1, itemTexto)
		
		if self.endereçoDasAlocações is not None:
			self.tabelaDados.setRowCount(0)
			for i, (end, pseudoOp, tam, lin) in enumerate(self.endereçoDasAlocações):
				numLinhas = self.tabelaDados.rowCount()			
				self.tabelaDados.insertRow(numLinhas)
				
				itemEnd = QtWidgets.QTableWidgetItem("0x" + hex(end)[2:].upper().zfill(4))
				self.tabelaDados.setItem(numLinhas, 0, itemEnd)
				self.itensDeDados[end] = i
				
				texto = ""
				for chave, valor in self.definiçõesDeRótulos.items():
					if valor[0] == end:
						texto = chave
				itemTexto = QtWidgets.QTableWidgetItem(texto)
				self.tabelaDados.setItem(numLinhas, 1, itemTexto)
				
				texto = self.cp.cCom.textoSimplificado.split("\n")[lin].lstrip()
				itemTexto = QtWidgets.QTableWidgetItem(texto)
				self.tabelaDados.setItem(numLinhas, 2, itemTexto)
				
				cod = ""
				for i in self.cp.cSim.memória[end:end+tam]:
					cod += hex(i)[2:].upper().zfill(2) + " "
				cod = cod[:-1]
				itemCod = QtWidgets.QTableWidgetItem(cod)
				self.tabelaDados.setItem(numLinhas, 3, itemCod)
		
		self.atualizaApontadorDePrograma(primeiroValor=True)
		#self.tabelaDados.resizeRowsToContents()
		#self.tabelaInstrucoes.resizeRowsToContents()

	def atualizaParâmetros(self, endereçosDosOperadores, endereçoDasAlocações, definiçõesDeRótulos):
		self.endereçosDosOperadores = endereçosDosOperadores
		self.endereçoDasAlocações = endereçoDasAlocações
		self.definiçõesDeRótulos = definiçõesDeRótulos


		