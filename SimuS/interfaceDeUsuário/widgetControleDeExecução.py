
from SimuS.interfaceDeUsuário.designer.ui_WidgetControleDeExec import Ui_widgetControleDeExec
from PyQt5.QtWidgets import QDockWidget
from PyQt5 import QtWidgets, QtGui

class WidgetControleDeExecução(QDockWidget, Ui_widgetControleDeExec):
	def __init__(self, cp, elementoPai=None):
		QDockWidget.__init__(self,elementoPai)

		self.setupUi(self)
		
		self.pegaEstado = lambda: cp.cSim.estado
		
		self.taxa = lambda: cp.cSim.taxa
		
		self.velocidadeSlider.valueChanged.connect(self.atualizaDeslizador)
		
		self.velocidadeSlider.setTracking(True)
		
		self.estadoValorLabel.setText("Parado")
		self.simulaButton.setEnabled(True)
		self.breakpointButton.setEnabled(True)
		self.pausaButton.setEnabled(False)
		self.paraButton.setEnabled(False)
		
	def atualizaDeslizador(self, val):
		if val == self.velocidadeSlider.minimum():
			self.velocidadeLabel.setText("Passo-a-passo")
		elif val == self.velocidadeSlider.maximum():
			self.velocidadeLabel.setText("Ilimitado")
		else:
			self.velocidadeLabel.setText(str(self.taxa()*val) + " instruções por segundo")
	
	def paintEvent(self, ev):
		QDockWidget.paintEvent(self, ev)
		opt = QtWidgets.QStyleOption()
		opt.initFrom(self)
		painter = QtGui.QPainter(self)                                                                                         
		self.style().drawPrimitive(QtWidgets.QStyle.PE_Widget, opt, painter, self)
		
	def atualizaEstado(self):
		estado = self.pegaEstado()
		if estado == "Executando":
			self.estadoValorLabel.setText("Executando")
			self.simulaButton.setEnabled(False)
			self.breakpointButton.setEnabled(False)
			self.pausaButton.setEnabled(True)
			self.paraButton.setEnabled(True)
		elif estado == "Pausado":
			self.estadoValorLabel.setText("Pausado")
			self.simulaButton.setEnabled(True)
			self.breakpointButton.setEnabled(True)
			self.pausaButton.setEnabled(False)
			self.paraButton.setEnabled(True)
		elif estado == "Parado":
			self.estadoValorLabel.setText("Parado")
			self.simulaButton.setEnabled(True)
			self.breakpointButton.setEnabled(True)
			self.pausaButton.setEnabled(False)
			self.paraButton.setEnabled(False)
		else:
			raise Exception()