
from SimuS.interfaceDeUsuário.designer.ui_WidgetEstadoDaMaquina import Ui_WidgetEstadoDaMaquina
from PyQt5.QtWidgets import QDockWidget
from PyQt5 import QtWidgets, QtGui

class WidgetEstadoDaMáquina(QDockWidget, Ui_WidgetEstadoDaMaquina):
	def __init__(self, elementoPai=None):
		QDockWidget.__init__(self, elementoPai)

		self.setupUi(self)
		
	def paintEvent(self, ev):
		QDockWidget.paintEvent(self, ev)
		opt = QtWidgets.QStyleOption()
		opt.initFrom(self)
		painter = QtGui.QPainter(self)                                                                                         
		self.style().drawPrimitive(QtWidgets.QStyle.PE_Widget, opt, painter, self)
		
		
	def atualizaValores(self, máquina):
		sp = máquina.apontadorDePilha & máquina.memória.endereçoMáximoDeMemória
		acc = máquina.acumulador & máquina.memória.valorMáximoDaPalavra
		pc = máquina.apontadorDeInstrução & máquina.memória.endereçoMáximoDeMemória
		n = máquina.negativeFlag
		z = máquina.zeroFlag
		c = máquina.carryFlag
		m = máquina.modoOitoBits
		self.spValorLabel.setText("0x" + format(sp, "04x").upper())
		self.spValorLabel.setToolTip(str(sp))
		self.accValorLabel.setText("0x" + format(acc, "04x").upper())
		self.accValorLabel.setToolTip(str(acc))
		self.pcValorLabel.setText("0x" + format(pc, "04x").upper())
		self.pcValorLabel.setToolTip(str(pc))
		if n:
			self.nValorLabel.setText("1")
		else:
			self.nValorLabel.setText("0")	
		if c:
			self.cValorLabel.setText("1")
		else:
			self.cValorLabel.setText("0")
		if z:
			self.zValorLabel.setText("1")
		else:
			self.zValorLabel.setText("0")
		if m:
			self.mValorLabel.setText("1")
		else:
			self.mValorLabel.setText("0")
		
		