
from SimuS.interfaceDeUsuário.designer.ui_WidgetConfig import Ui_WidgetConfig
from PyQt5.QtWidgets import QDialog, QColorDialog, QDialogButtonBox
from PyQt5.QtGui import QFont, QPalette, QFontMetrics, QColor

class WidgetConfigurações(QDialog, Ui_WidgetConfig):
    def __init__(self,cp, elementoPai=None):
        self.cp = cp     
        QDialog.__init__(self, elementoPai)
        self.setupUi(self)
        
        self.confirmacaoButtonBox.accepted.connect(self.confirma)
        self.confirmacaoButtonBox.button(QDialogButtonBox.Apply).clicked.connect(self.confirma)
        
        self.rotuloCor.clicked.connect(lambda: self.botãoEscolhaDeCor("rótulo", self.rotuloCor))
        self.numeralCor.clicked.connect(lambda: self.botãoEscolhaDeCor("numeral", self.numeralCor))
        self.stringCor.clicked.connect(lambda: self.botãoEscolhaDeCor("texto", self.stringCor))
        self.operadorCor.clicked.connect(lambda: self.botãoEscolhaDeCor("operador", self.operadorCor))
        self.comentarioCor.clicked.connect(lambda: self.botãoEscolhaDeCor("comentário", self.comentarioCor))
        self.diretivaCor.clicked.connect(lambda: self.botãoEscolhaDeCor("diretiva", self.diretivaCor))
        
    def atualizaInterface(self):
        c = self.cp.conf
        
        #Editor
        self.atualizaCorDosBotões()
        
        fonteFam, fonteTam = c["editor"]["fonte"], float(c["editor"]["tamanho da fonte"])
        índice = self.editorFonte.findText(fonteFam)
        self.editorFonte.setCurrentIndex(índice)
        self.tamanhoFonteEditor.setValue(fonteTam)
        
        tabTamanho = int(c["editor"]["largura da tabulação"])
        self.larguraDaTabulacaoEditor.setValue(tabTamanho)
    
        #Geral
        self.iconesAlternativosCheckBox.setChecked(
            bool(int(c["geral"]["ícones alternativos"]))
        )   
        self.alternarAbaCheckBox.setChecked(
            bool(int(c["geral"]["alternar aba"]))
        )        
        self.instrucoesCheckBox.setChecked(
            bool(int(c["geral"]["acompanha instrução"]))
        )        
        self.pilhaCheckBox.setChecked(
            bool(int(c["geral"]["acompanha pilha"]))
        )
        
        #Console
        self.numeroMaximoDeMensagens.setValue(
            int(c["console"]["máximo de mensagens"])
        )
        self.alertaCheckBox.hide()
        self.alertaCheckBox.setChecked(
            bool(int(c["console"]["alertar ao executar dados"]))
        )
        self.limparAvisosCheckBox.setChecked(
            bool(int(c["console"]["limpar avisos"])) 
        )
        fonteFam, fonteTam = c["console"]["fonte"], float(c["console"]["tamanho da fonte"])
        índice = self.fonteConsole.findText(fonteFam)
        self.fonteConsole.setCurrentIndex(índice)
        self.tamanhoFonteConsole.setValue(int(fonteTam))
        
    def atualizaObjetoDeConfigurações(self):
        """Atualiza os valores do objeto de configurações utilizando os valores desta widget"""        
        c = self.cp.conf
        
        c["editor"]["tamanho da fonte"] = str(self.tamanhoFonteEditor.value())
        c["editor"]["fonte"] = self.editorFonte.currentFont().family()

        c["editor"]["largura da tabulação"] = str(self.larguraDaTabulacaoEditor.value())
    
        #Geral
        c["geral"]["ícones alternativos"] = '1' if self.iconesAlternativosCheckBox.isChecked() else '0'
        c["geral"]["alternar aba"] = '1' if self.alternarAbaCheckBox.isChecked() else '0'
        c["geral"]["acompanha instrução"] = '1' if self.instrucoesCheckBox.isChecked() else '0'
        c["geral"]["acompanha pilha"] = '1' if self.pilhaCheckBox.isChecked() else '0'
        
        #Console
        c["console"]["máximo de mensagens"] = str(self.numeroMaximoDeMensagens.value())
        c["console"]["alertar ao executar dados"] = '1' if self.alertaCheckBox.isChecked() else '0'
        c["console"]["limpar avisos"] = '1' if self.limparAvisosCheckBox.isChecked() else '0'
        
        c["console"]["fonte"] = self.fonteConsole.currentFont().family()
        c["console"]["tamanho da fonte"] = str(self.tamanhoFonteConsole.value())
        
        c.cores = self.cores
        
    def atualizaCorDosBotões(self):
        """Carrega as a cores nos botões de coloração da fonte, e cria um dicionário pra armazenar as alterações."""   
        c = self.cp.conf
        
        self.cores = c.cores.copy()
        
        pal = self.rotuloCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["rótulo"]))
        self.rotuloCor.setAutoFillBackground(True)
        self.rotuloCor.setPalette(pal)
        self.rotuloCor.update()
        
        pal = self.numeralCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["numeral"]))
        self.numeralCor.setAutoFillBackground(True)
        self.numeralCor.setPalette(pal)
        self.numeralCor.update()
        
        pal = self.stringCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["texto"]))
        self.stringCor.setAutoFillBackground(True)
        self.stringCor.setPalette(pal)
        self.stringCor.update()
        
        pal = self.operadorCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["operador"]))
        self.operadorCor.setAutoFillBackground(True)
        self.operadorCor.setPalette(pal)
        self.operadorCor.update()
        
        pal = self.comentarioCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["comentário"]))
        self.comentarioCor.setAutoFillBackground(True)
        self.comentarioCor.setPalette(pal)
        self.comentarioCor.update()
        
        pal = self.diretivaCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores["diretiva"]))
        self.diretivaCor.setAutoFillBackground(True)
        self.diretivaCor.setPalette(pal)
        self.diretivaCor.update()
        
    def show(self):
        self.atualizaInterface()
        QDialog.show(self,)
        
    def botãoEscolhaDeCor(self, objNome, obj):
        cd = QColorDialog()        
        cor = cd.getColor(QColor(*self.cores[objNome]))
        self.cores[objNome] = (cor.red(), cor.green(), cor.blue())
        
        pal = self.diretivaCor.palette()
        pal.setColor(QPalette().Button, QColor(*self.cores[objNome]))
        obj.setAutoFillBackground(True)
        obj.setPalette(pal)
        obj.update()
        
    def confirma(self):
        self.atualizaObjetoDeConfigurações()
        self.cp.conf.aplicaConfigurações()
        
        