
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import pyqtSlot

class EditorDeTexto:
	def pegaTexto(self):
		return self.editorDeCodigo.toPlainText()
	
class WidgetEditorDeTexto(QtWidgets.QPlainTextEdit):
	def __init__(self, parent=None):
		super(WidgetEditorDeTexto,self).__init__(parent)
		self.areaNum = ÁreaComNumeração(self)		
		self.setSizePolicy(QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Expanding )
		self.blockCountChanged.connect(self.atualizaEspessura)
		self.updateRequest.connect(self.atualizaAreaDeNumeracao)
		self.cursorPositionChanged.connect(self.destacaLinhaAtual)
		
		#Altera fonte
		fonte = QtGui.QFont("Monospace")
		fonte.setStyleHint(QtGui.QFont.TypeWriter)
		fonte.setFixedPitch(True)
		fonte.setPointSize(11)
		self.setFont(fonte)
		
		tamanhoDaTabulação = 4

		medida = QtGui.QFontMetrics(fonte)
		self.setTabStopWidth(tamanhoDaTabulação * medida.width(' '))

		self.atualizaEspessura(0)

	def númeroDeLinhaEventoPintura(self, evento):
		painter = QtGui.QPainter(self.areaNum)
		painter.fillRect(evento.rect(), QtGui.QColor(QtCore.Qt.lightGray).lighter(120))
		altura = int(self.fontMetrics().height())
	
		bloco = self.firstVisibleBlock()
		númeroDoBloco = bloco.blockNumber()
		topo = int(self.blockBoundingGeometry(bloco).translated(self.contentOffset()).top())
		fundo = int(topo + self.blockBoundingRect(bloco).height())
				
		while bloco.isValid() and (topo <= evento.rect().bottom()):
			if bloco.isVisible() and (fundo >= evento.rect().top()):
				number = str(númeroDoBloco + 1)
				painter.setPen(QtCore.Qt.black)
				painter.drawText(0, topo, self.areaNum.width(), altura,
								 QtCore.Qt.AlignCenter, number)
			bloco = bloco.next()
			topo = fundo
			fundo = int(topo + self.blockBoundingRect(bloco).height())
			númeroDoBloco += 1
			
	@pyqtSlot()
	def destacaLinhaAtual(self):
		outrasSeleções = []

		if not self.isReadOnly():
			seleção = QtWidgets.QTextEdit.ExtraSelection()
			
			lineColor = QtGui.QColor(QtCore.Qt.blue).lighter(192)
			
			seleção.format.setBackground(lineColor)
			seleção.format.setProperty(QtGui.QTextFormat.FullWidthSelection, True)
			seleção.cursor = self.textCursor()
			seleção.cursor.clearSelection()
			outrasSeleções.append(seleção)
		self.setExtraSelections(outrasSeleções)

	@pyqtSlot(int)
	def atualizaEspessura(self, _):
		self.setViewportMargins(self.espessuraDaÁreaDeNumeração(), 0, 0, 0)

	def espessuraDaÁreaDeNumeração(self):
		dígitos = 1
		contador = max(1, self.blockCount())
		while contador >= 10:
			contador /= 10
			dígitos += 1
		espaço = 6 + self.fontMetrics().width('9') * dígitos
		return espaço
	
	@pyqtSlot(QtCore.QRect, int)
	def atualizaAreaDeNumeracao(self, rect, dy): #Sem acentos devido ao pyqtSlot
		if dy:
			self.areaNum.scroll(0, dy)
		else:
			self.areaNum.update(0, rect.y(), self.areaNum.width(),
								   rect.height())

		if rect.contains(self.viewport().rect()):
			self.atualizaEspessura(0)


	def resizeEvent(self, event):
		super().resizeEvent(event)
		
		cr = self.contentsRect();
		self.areaNum.setGeometry(QtCore.QRect(cr.left(), cr.top(),
										self.espessuraDaÁreaDeNumeração(), cr.height()))
	
class ÁreaComNumeração(QtWidgets.QWidget):
	def __init__(self, editor):
		assert(isinstance(editor,WidgetEditorDeTexto))
		super(ÁreaComNumeração, self).__init__(editor)
		self.editorDeCódigo = editor
	def sizeHint(self):
		return QtCore.QSize(self.editor.espessuraDaÁreaDeNumeração(), 0)
	def paintEvent(self, evento):
		self.editorDeCódigo.númeroDeLinhaEventoPintura(evento)
	