
from PyQt5.QtGui import QColor, QTextCharFormat, QFont, QBrush

from SimuS.compilador import AnalisadorLéxico
from SimuS.interfaceDeUsuário.destaqueDeSintaxeBase import DestaqueDeSintaxeBase, Regra

class DestaqueDeSintaxe(DestaqueDeSintaxeBase):
	corOperador = QColor(0,0,180)
	corDiretiva = QColor(120,120,255)
	corNumeral = QColor(180,0,0)
	corRótulo = QColor(0,180,0)
	corComentário = QColor(180,180,180)
	corTexto = QColor(255,153,0)
	
	
	def defineFormatação(self):
		"""Cria os objetos de formatação a partir das cores de formatação no objeto"""
		self.formataçãoDoOperador = QTextCharFormat()
		self.formataçãoDoOperador.setForeground( QBrush(self.corOperador))
		self.formataçãoDoOperador.setFontWeight( QFont.Bold )
		
		self.formataçãoDaDiretiva = QTextCharFormat()
		self.formataçãoDaDiretiva.setForeground( QBrush(self.corDiretiva))
		self.formataçãoDaDiretiva.setFontWeight( QFont.Bold )
			
		self.formataçãoDoNumeral = QTextCharFormat()
		self.formataçãoDoNumeral.setForeground( QBrush(self.corNumeral))
		self.formataçãoDoNumeral.setFontWeight( QFont.Bold )
		
		self.formataçãoDoRótulo = QTextCharFormat()
		self.formataçãoDoRótulo.setForeground( QBrush(self.corRótulo))
		self.formataçãoDoRótulo.setFontWeight( QFont.Bold )
		
		self.formataçãoDoComentário = QTextCharFormat()
		self.formataçãoDoComentário.setForeground( QBrush(self.corComentário))
		self.formataçãoDoComentário.setFontWeight( QFont.Bold )
		
		self.formataçãoDoTexto = QTextCharFormat()
		self.formataçãoDoTexto.setForeground( QBrush(self.corTexto))
		self.formataçãoDoTexto.setFontWeight( QFont.Bold )
			
	
	def __init__(self, elementoPai, documento):
		DestaqueDeSintaxeBase.__init__(self, elementoPai, documento)		
		self.defineFormatação()
		self.defineRegras()
		
	def defineRegras(self):		
		al = AnalisadorLéxico("")
		self.regras = []
		regraOperadores = "(^|\s)("
		for (padrão, arg1, arg2) in al.operadores:
			regraOperadores += padrão + "|"
		regraOperadores = regraOperadores[:-1] + ")($|(?=\s)|(?=;))"
		self.regras += [Regra(regraOperadores, self.formataçãoDoOperador)]
		
		regraRótulo = "(?!" + regraOperadores + ")((^|\s)("
		for (padrão, arg1, arg2) in al.rotulos:
			regraRótulo += padrão + "|" 
		regraRótulo = regraRótulo[:-1] + ")((?=\+)|$|(?=\s)|(?=;)))"
		self.regras += [Regra(regraRótulo, self.formataçãoDoRótulo)]
		
		regraNumeral = "(^|\s|,|\+)("
		for (padrão, arg1, arg2) in al.numeros:
			regraNumeral += padrão + "|"
		regraNumeral = regraNumeral[:-1] + ")($|(?=\s)|(?=;)|(?=,))"
		self.regras += [Regra(regraNumeral, self.formataçãoDoNumeral)]

		regraDeslocamento = "\+($|(?=\s)|(?=;))"
		self.regras += [Regra(regraDeslocamento, self.formataçãoDoNumeral)]
		
		regraTexto = "(^|\s|,)("
		for (padrão, arg1, arg2) in al.cadeiasDeCaracteres:
			regraTexto += padrão + "|" + padrão[:-1] + "|" #Isso remove a aspas final, permitindo que o programa "preveja" a string. Se possível, alterar essa operação para algo mais organizado
		regraTexto = regraTexto[:-1] + ")($|(?=\s)|(?=;)|,)"
		self.regras += [Regra(regraTexto, self.formataçãoDoTexto)]
		
		regraVírgula = "(^|\s),($|(?=\s)|(?=;))"		
		self.regras += [Regra(regraVírgula, self.formataçãoDoNumeral)]
		
		regraDiretiva = "(^|\s)("
		for (padrão, arg1, arg2) in al.diretivas:
			regraDiretiva += padrão + "|"
		regraDiretiva = regraDiretiva[:-1] + ")($|(?=\s)|(?=;))"
		self.regras += [Regra(regraDiretiva, self.formataçãoDaDiretiva)]
		
		regraComentário = ";.*($|\s)"
		self.regras += [Regra(regraComentário, self.formataçãoDoComentário)]			