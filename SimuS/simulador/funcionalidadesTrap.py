import time
from math import pi, sin, floor
from PyQt5.QtMultimedia import QAudioOutput, QAudioFormat, QAudioDeviceInfo, QAudio
from PyQt5.QtCore import QBuffer, QByteArray, QIODevice, QEventLoop

class FuncionalidadesTrap:
    console = None
    def __init__(self, cp):
        self.processaEventos = lambda: cp.app.processEvents()
        self.estadoDeExecução = lambda: cp.cSim.estado
    
    def associaConsole(self, console):
        self.console = console
        
    def desassociaConsole(self,):
        console = self.console
        self.console = None
        return console
  
    def escritaNoConsole(self, valor):
        if type(valor) == int:
            valor = [valor]
        texto = bytes(valor).decode('iso-8859-1', errors="ignore")
        self.console.escreveNoConsole(texto)
    
    def leituraDoConsole(self, cadeiaDeCaracteres):
        while self.console.textoNoBuffer == "" and self.estadoDeExecução != "Parado":
            self.processaEventos()
        txt = self.console.consomeBuffer(cadeiaDeCaracteres)
        dado = bytearray(txt, encoding="iso-8859-1", errors="ignore")
        dado.append(0)   
        return dado if cadeiaDeCaracteres else dado[0]
    
    def esperaTempo(self, valor):
        tempoAEsperar = valor/1000
        primeiraMedição = time.time()
        while ((time.time() - primeiraMedição ) < tempoAEsperar) and self.estadoDeExecução() != "Parado":
            self.processaEventos()
        
    def tocaSom(self, altura, duração):
        maxAmp = 255/2
        taxaAmostral = 44100
        constFreq = (2*pi)/taxaAmostral
        freq = 440* (2 ** ((altura/2 - 45)*(1/12)))
        segundos = duração/20
        
        formato = QAudioFormat()
        
        formato.setSampleRate(taxaAmostral);
        formato.setChannelCount(1);
        formato.setSampleSize(8);
        formato.setCodec("audio/pcm");
        formato.setByteOrder(QAudioFormat.LittleEndian)
        formato.setSampleType(QAudioFormat.UnSignedInt)
        
        info = QAudioDeviceInfo(QAudioDeviceInfo.defaultOutputDevice())
        
        
        
        bytesSom = bytearray([0 for i in range(int(segundos * taxaAmostral))])
        for i in range(int(segundos * taxaAmostral)):
            val = freq*i*constFreq
            val = floor((sin(val)+1) * maxAmp)
            bytesSom[i]=val
        
        bytesSom = QByteArray(bytesSom)
        
        buffer = QBuffer(bytesSom)
        buffer.open(QIODevice.ReadOnly)

        audio = QAudioOutput(formato)
        
        if not info.isFormatSupported(formato):
            print("Formato não suportado.")
        
        if audio.error() != QAudio.NoError:
            print("erro: ", audio.error())

        audio.start(buffer)
        
        loop = QEventLoop()
        audio.stateChanged.connect(loop.quit)
        
        loop.exec()
        self.processaEventos()
        
        while audio.state() == QAudio.ActiveState:
            loop.exec()
            self.processaEventos()
             
    def pseudoAleatório(self, semente):
        return ((semente%100)*21 + 63) % 100
    
    