
from SimuS.simulador.máquinaBase import MáquinaBase
from SimuS.simulador.exceções import ExceçãoDeOperandoInválido, ExceçãoTRAPInválido
from SimuS.simulador.funcionalidadesTrap import FuncionalidadesTrap
from SimuS.simulador.parâmetros import tamanhoDoEndereço
from SimuS.simulador.pinos import Pinos


class Máquina(MáquinaBase, FuncionalidadesTrap, Pinos):
	def __init__(self, memória, cp):
		FuncionalidadesTrap.__init__(self, cp)
		Pinos.__init__(self)
		MáquinaBase.__init__(self, memória)
		
	def passoNOP(self, tipoDoOperando):		
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("NOP", tipoDoOperando, self.apontadorDeInstrução)

	def passoSEP(self, tipoDoOperando):		
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("NOP", tipoDoOperando, self.apontadorDeInstrução)
		self.modoOitoBits = True

	def passoREP(self, tipoDoOperando):		
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("NOP", tipoDoOperando, self.apontadorDeInstrução)
		self.modoOitoBits = False
			
	def passoSTA(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("STA", tipoDoOperando, self.apontadorDeInstrução)
		
		if self.modoOitoBits:
			self.memória[operando:operando + self.memória.tamanhoDaPalavraOitoBits] = self.parteBaixaDoAcumulador.to_bytes(self.memória.tamanhoDaPalavraOitoBits, self.memória.ordemDosBytes)
		else:
			self.memória[operando:operando +  self.memória.tamanhoDaPalavra] = self.acumulador.to_bytes( self.memória.tamanhoDaPalavra, self.memória.ordemDosBytes)
	
	def passoXGSP(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("STS", tipoDoOperando, self.apontadorDeInstrução)
			
		self.apontadorDePilha, self.acumulador = self.acumulador, self.apontadorDePilha

	def passoXGAC(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("STS", tipoDoOperando, self.apontadorDeInstrução)
			
		self.acumulador = (self.parteBaixaDoAcumulador << 8) + (self.parteAltaDoAcumulador >> 8)

	def passoLDA(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("LDA", tipoDoOperando, self.apontadorDeInstrução)
		if self.modoOitoBits:
			self.acumulador = operando + self.parteAltaDoAcumulador
		else:
			self.acumulador = operando
		self.atualizaFlags()
		
	def passoLDI(self, tipoDoOperando):
		self.passoLDA("imediato")
	
	def passoADD(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("ADD", tipoDoOperando, self.apontadorDeInstrução)
		if self.modoOitoBits:
			self.carryFlag = (self.parteBaixaDoAcumulador + operando) > self.memória.valorMáximoDaPalavraOitoBits
			self.acumulador = self.parteAltaDoAcumulador + ((self.parteBaixaDoAcumulador + operando) & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			self.carryFlag = (self.acumulador + operando) > self.memória.valorMáximoDaPalavra
			self.acumulador = (self.acumulador + operando) & self.memória.valorMáximoDaPalavra
		self.atualizaFlags()
	
	def passoADC(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)			
		else:
			raise ExceçãoDeOperandoInválido("ADC", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			valorSoma = (self.parteBaixaDoAcumulador + operando + self.carryFlag)
			self.carryFlag = valorSoma > self.memória.valorMáximoDaPalavraOitoBits
			self.acumulador = self.parteAltaDoAcumulador + (valorSoma & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			valorSoma = self.acumulador + operando + self.carryFlag
			self.carryFlag = valorSoma > self.memória.valorMáximoDaPalavra
			self.acumulador = valorSoma & self.memória.valorMáximoDaPalavra

		self.atualizaFlags()
		
	def passoSUB(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("SUB", tipoDoOperando, self.apontadorDeInstrução)
		
		if self.modoOitoBits:
			valorSubtração = (self.parteBaixaDoAcumulador - operando)
			self.carryFlag = valorSubtração < 0 
			self.acumulador = self.parteAltaDoAcumulador + (valorSubtração & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			self.carryFlag = (self.acumulador - operando) < 0
			self.acumulador = (self.acumulador - operando) & self.memória.valorMáximoDaPalavra


		self.atualizaFlags()
	
	def passoSBC(self,tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("SBC", tipoDoOperando, self.apontadorDeInstrução)
		if self.carryFlag:
			operando += 1

		if self.modoOitoBits:
			valorSubtração = (self.parteBaixaDoAcumulador - operando - self.carryFlag)
			self.carryFlag = valorSubtração < 0 
			self.acumulador = self.parteAltaDoAcumulador + (valorSubtração & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			valorSubtração = (self.acumulador - operando - self.carryFlag)
			self.carryFlag = valorSubtração < 0
			self.acumulador = valorSubtração & self.memória.valorMáximoDaPalavra

		self.atualizaFlags()
		
	def passoOR(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("OR", tipoDoOperando, self.apontadorDeInstrução)

		#A busca de operando carrega o número correto de bits, e no caso dessa operação isso basta para garantir corretude nos modos de 16/8 bits
		self.acumulador = self.acumulador | operando

		self.atualizaFlags()
	
	def passoXOR(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("XOR", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			self.acumulador = self.parteAltaDoAcumulador + (self.parteBaixaDoAcumulador ^ operando)
		else:
			self.acumulador = self.acumulador ^ operando
		self.atualizaFlags()
		
	def passoAND(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoDireto(True)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoIndireto(True)
		elif tipoDoOperando == "imediato":
			operando = self.buscaOperandoImediato(True)
		else:
			raise ExceçãoDeOperandoInválido("AND", tipoDoOperando, self.apontadorDeInstrução)
		
		if self.modoOitoBits:
			self.acumulador = self.parteAltaDoAcumulador + (self.parteBaixaDoAcumulador & operando)
		else:
			self.acumulador = self.acumulador & operando
		self.atualizaFlags()
	
	def passoNOT(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("NOT", tipoDoOperando, self.apontadorDeInstrução)
		self.acumulador = (self.acumulador ^ self.valorMáximoDaPalavraAtual)
		self.atualizaFlags()
	
	def passoSHL(self, tipoDoOperando):		
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("SHL", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			self.carryFlag = (self.parteBaixaDoAcumulador & 2**(self.memória.tamanhoDaPalavraOitoBits *8 -1)) != 0
			self.acumulador = self.parteAltaDoAcumulador + ((self.parteBaixaDoAcumulador << 1) & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			self.carryFlag = (self.acumulador & 2**(self.memória.tamanhoDaPalavra *8 -1)) != 0
			self.acumulador = (self.acumulador << 1) & self.memória.valorMáximoDaPalavra
		
		self.atualizaFlags()
		
	def passoSHR(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("SHR", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			self.carryFlag = bool(self.acumulador & 1)
			self.acumulador = self.parteAltaDoAcumulador + ((self.parteBaixaDoAcumulador >> 1) & self.memória.valorMáximoDaPalavraOitoBits)
		else:
			self.carryFlag = bool(self.acumulador & 1)
			self.acumulador = (self.acumulador >> 1) & self.memória.valorMáximoDaPalavra
		self.atualizaFlags()
		
	def passoSRA(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("SRA", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			self.carryFlag = bool(self.parteBaixaDoAcumulador & 1)
			msb = (self.parteBaixaDoAcumulador & 2**(self.memória.tamanhoDaPalavraOitoBits *8 -1))
			self.acumulador = ((msb + (self.parteBaixaDoAcumulador >> 1)) & self.memória.valorMáximoDaPalavraOitoBits)+ self.parteAltaDoAcumulador
		else:
			self.carryFlag = bool(self.acumulador & 1)
			msb = (self.acumulador & 2**(self.memória.tamanhoDaPalavra *8 -1))
			self.acumulador = (msb + (self.acumulador >> 1)) & self.memória.valorMáximoDaPalavra
		self.atualizaFlags()
	
	def passoJMP(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JMP", tipoDoOperando, self.apontadorDeInstrução)
		self.apontadorDeInstrução = operando
	
	def passoJN(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JN", tipoDoOperando, self.apontadorDeInstrução)
		if self.negativeFlag:
			self.apontadorDeInstrução = operando
			
	def passoJP(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JP", tipoDoOperando, self.apontadorDeInstrução)
		if not self.negativeFlag and not self.zeroFlag: 
			self.apontadorDeInstrução = operando
			
	def passoJZ(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JZ", tipoDoOperando, self.apontadorDeInstrução)
		if self.zeroFlag: 
			self.apontadorDeInstrução = operando
			
	def passoJNZ(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JNZ", tipoDoOperando, self.apontadorDeInstrução)
		if not self.zeroFlag: 
			self.apontadorDeInstrução = operando
			
	def passoJC(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JC", tipoDoOperando, self.apontadorDeInstrução)			
		if self.carryFlag:
			self.apontadorDeInstrução = operando
			
	def passoJNC(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JC", tipoDoOperando, self.apontadorDeInstrução)			
		if not self.carryFlag:
			self.apontadorDeInstrução = operando
	
	def passoJSR(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("JSR", tipoDoOperando, self.apontadorDeInstrução)
		self.apontadorDePilha -= self.memória.tamanhoDoEndereço
		self.apontadorDePilha &= self.memória.endereçoMáximoDeMemória
		self.memória[
			self.apontadorDePilha:
			self.apontadorDePilha + self.memória.tamanhoDoEndereço
		] = self.apontadorDeInstrução.to_bytes(self.memória.tamanhoDoEndereço, self.memória.ordemDosBytes)
		self.apontadorDeInstrução = operando
		
	def passoRET(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("RET", tipoDoOperando, self.apontadorDeInstrução)
		self.apontadorDeInstrução = int.from_bytes(self.memória[self.apontadorDePilha: self.apontadorDePilha + self.memória.tamanhoDoEndereço],
								self.memória.ordemDosBytes)
		self.apontadorDePilha += self.memória.tamanhoDoEndereço
		self.apontadorDePilha &=  self.memória.endereçoMáximoDeMemória
		
	def passoPUSH(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("PUSH", tipoDoOperando, self.apontadorDeInstrução)

		if self.modoOitoBits:
			self.apontadorDePilha -= self.memória.tamanhoDaPalavraOitoBits
			self.apontadorDePilha &= self.memória.endereçoMáximoDeMemória
			self.memória[self.apontadorDePilha:
						self.apontadorDePilha + self.memória.tamanhoDaPalavraOitoBits
						] = self.parteBaixaDoAcumulador.to_bytes(self.memória.tamanhoDaPalavraOitoBits, self.memória.ordemDosBytes)
		else:
			self.apontadorDePilha -= self.memória.tamanhoDaPalavra
			self.apontadorDePilha &= self.memória.endereçoMáximoDeMemória
			self.memória[self.apontadorDePilha:
						self.apontadorDePilha + self.memória.tamanhoDaPalavra 
						] = self.acumulador.to_bytes(self.memória.tamanhoDaPalavra, self.memória.ordemDosBytes)

	def passoPOP(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("POP", tipoDoOperando, self.apontadorDeInstrução)
		if self.modoOitoBits:
			self.acumulador = self.parteAltaDoAcumulador + (self.memória[self.apontadorDePilha] & self.memória.valorMáximoDaPalavraOitoBits)
			self.apontadorDePilha += self.memória.tamanhoDaPalavraOitoBits
			self.apontadorDePilha &= self.memória.endereçoMáximoDeMemória
		else:
			self.acumulador = int.from_bytes(self.memória[self.apontadorDePilha:self.apontadorDePilha+tamanhoDoEndereço], self.memória.ordemDosBytes)
			self.apontadorDePilha += self.memória.tamanhoDaPalavra 
			self.apontadorDePilha &= self.memória.endereçoMáximoDeMemória
		self.atualizaFlags()
		
	def passoTRAP(self, tipoDoOperando):
		if tipoDoOperando is None:
			operando = self.buscaOperandoImediato(False)
		elif tipoDoOperando == "indireto":
			operando = self.buscaOperandoDireto(False)
		else:
			raise ExceçãoDeOperandoInválido("TRAP", tipoDoOperando, self.apontadorDeInstrução)
		if self.acumulador == 1:
			self.acumulador = self.leituraDoConsole(cadeiaDeCaracteres=False)
		elif self.acumulador == 2:
			dado = self.memória[operando]
			self.escritaNoConsole(valor=dado)
		elif self.acumulador == 3:
			dados = bytearray(self.leituraDoConsole(cadeiaDeCaracteres=True))
			self.memória[operando:operando+len(dados)] = dados
		elif self.acumulador == 4:
			dados = [self.memória[i+operando] for i in range(256)]
			if 0 in dados:
				pos = dados.index(0)
				dados = dados[:pos]
			self.escritaNoConsole(valor=dados)
		elif self.acumulador == 5:
			dados = self.memória[
					operando:
					operando+self.memória.tamanhoDaPalavra
				]
			valor = int.from_bytes(
					dados,
					self.memória.ordemDosBytes
				)
			self.esperaTempo(valor=valor)
		elif self.acumulador == 6:
			altura = self.memória[operando]
			duração = self.memória[operando+1]
			self.tocaSom(altura=altura, duração=duração)
		elif self.acumulador == 7:
			dados = self.memória[
					operando:
					operando+self.memória.tamanhoDaPalavra
				]
			valor = int.from_bytes(
					dados,
					self.memória.ordemDosBytes
				)
			self.acumulador = self.pseudoAleatório(semente=valor)
		elif self.acumulador == 101:
			pino = self.memória[operando]
			modo = self.memória[operando+1]
			self.configura(pino, modo)
		elif self.acumulador == 102:
			pino = self.memória[operando]
			valor = self.memória[operando+1]
			self.escreveValor(pino, valor)
		elif self.acumulador == 103:
			pino = self.memória[operando]
			self.acumulador = 1 if self.pegaValor(pino) else 0						
		elif self.acumulador == 104:			
			pino = self.memória[operando]			
			valor = self.memória[operando+1]
			self.configuraPullUpPullDown(pino, valor)
		elif self.acumulador == 105:
			pino = self.memória[operando]			
			valor = self.memória[operando+1]
			self.configuraDC(pino, valor)
		else:
			raise ExceçãoTRAPInválido(self.acumulador, self.apontadorDeInstrução)

		
	def passoHLT(self, tipoDoOperando):
		if tipoDoOperando is not None:
			raise ExceçãoDeOperandoInválido("HLT", tipoDoOperando, self.apontadorDeInstrução)
		self.aumentaApontadorDeInstrução(-self.tamanhoDaInstrução)

