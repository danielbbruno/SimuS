

class ExceçãoDeOpcodeInválido(Exception):
	def __init__(self, posição, códigoDoOperador):
		self.posição = posição
		self.códigoDoOperador = códigoDoOperador
		self.mensagem = "Operador inválido na posição de memória 0x" + hex(self.posição)[2:].zfill(4) + " (código do operador " + str(self.códigoDoOperador) + ")."
		super(ExceçãoDeOpcodeInválido, self).__init__(self.mensagem)	
	
class ExceçãoDeOperandoInválido(Exception):
	def __init__(self, instrução, tipoDoOperando, posição):
		self.instrução = instrução
		self.tipoDoOperando = tipoDoOperando
		self.posição = posição
		self.mensagem = "Operador inválido na posição de memória 0x" + hex(self.posição)[2:].zfill(4) + " (operador "+ str(instrução) + " com operando "+ str(tipoDoOperando) + ")."
		super(ExceçãoDeOperandoInválido, self).__init__(self.mensagem)	
	
class ExceçãoTRAPInválido(Exception):
	def __init__(self, número, posição):
		self.número = número
		self.posição = posição
		self.mensagem = "Ao tentar executar o operador TRAP, na posição de memória 0x" + hex(self.posição)[2:].zfill(4) 
		self.mensagem += ", o simulador encontrou um valor inválido de operando implícito no acumulador: " + str(self.número) + "."
		super(ExceçãoTRAPInválido, self).__init__(self.mensagem)	
	
class ExceçãoTRAPIndisponível(Exception):
	def __init__(self, mensagem=None):
		if mensagem is None:
			self.mensagem = """Biblioteca para pinos de E/S do Raspberry Pi não está disponível."""
		else:
			self.mensagem = mensagem
		Exception.__init__(self, self.mensagem)
		
		