
from SimuS.simulador.parâmetros import códigoDosOperandos, códigosDosOperandosInvertido, permiteOperandoIndireto, permiteOperandoImediato
from SimuS.simulador.exceções import ExceçãoDeOpcodeInválido
from SimuS.simulador.parâmetros import tamanhoDaInstrução


class MáquinaBase:
	def __init__(self, memória, acumulador=0, apontadorDeInstrução = 0, apontadorDePilha = 0, zeroFlag=False, negativeFlag=False, carryFlag=False, modoOitoBits=False):
		self.memória = memória
		self.apontadorDeInstrução = apontadorDeInstrução
		self.acumulador = acumulador
		self.zeroFlag = zeroFlag
		self.carryFlag = carryFlag
		self.negativeFlag = negativeFlag

		self.modoOitoBits = modoOitoBits

		self.apontadorDePilha = apontadorDePilha
		self.funçãoDeAtualização = None
		self.tamanhoDaInstrução = tamanhoDaInstrução

	@property
	def tamanhoDaPalavraAtual(self,):
		if self.modoOitoBits:
			return self.memória.tamanhoDaPalavraOitoBits 
		else:
			return self.memória.tamanhoDaPalavra

	@property
	def valorMáximoDaPalavraAtual(self,):
		if self.modoOitoBits:
			return self.memória.valorMáximoDaPalavraOitoBits 
		else:
			return self.memória.valorMáximoDaPalavra

	@property
	def parteAltaDoAcumulador(self,):
		return self.acumulador & (self.memória.valorMáximoDaPalavra - self.memória.valorMáximoDaPalavraOitoBits)

	@property
	def parteBaixaDoAcumulador(self,):
		return self.acumulador & self.memória.valorMáximoDaPalavraOitoBits
	
	def zeraEstado(self):
		self.apontadorDeInstrução = 0
		self.acumulador = 0
		self.zeroFlag = False
		self.carryFlag = False
		self.negativeFlag = False
		self.modoOitoBits = False
		self.apontadorDePilha = 0
	
	def passo(self):
		byteDoOperador = int(self.memória[self.apontadorDeInstrução])
		
		if byteDoOperador in códigoDosOperandos.values():
			stringDoOperador = códigosDosOperandosInvertido[byteDoOperador]
			tipoDoOperando = None
		elif (byteDoOperador - 1) in códigoDosOperandos.values():
			stringDoOperador = códigosDosOperandosInvertido[byteDoOperador - 1]
			tipoDoOperando = "indireto"
			if stringDoOperador not in permiteOperandoIndireto:
				raise ExceçãoDeOpcodeInválido(self.apontadorDeInstrução, byteDoOperador)
		elif (byteDoOperador - 2) in códigoDosOperandos.values():
			stringDoOperador = códigosDosOperandosInvertido[byteDoOperador - 2]
			tipoDoOperando = "imediato"
			if stringDoOperador not in permiteOperandoImediato:
				raise ExceçãoDeOpcodeInválido(self.apontadorDeInstrução, byteDoOperador)
		else:
			raise ExceçãoDeOpcodeInválido(self.apontadorDeInstrução, byteDoOperador)
		
		self.aumentaApontadorDeInstrução(self.tamanhoDaInstrução)
		passo = getattr(self, "passo" + stringDoOperador)
		passo(tipoDoOperando)
		return stringDoOperador
		
	def buscaOperandoDireto(self, palavra):
		if palavra:
			numBytes = self.tamanhoDaPalavraAtual
		else:
			numBytes = self.memória.tamanhoDoEndereço
		
		bytesDaVariável = self.memória[self.apontadorDeInstrução: self.apontadorDeInstrução + self.memória.tamanhoDoEndereço]
		endereçoDaVariável = int.from_bytes(bytesDaVariável, self.memória.ordemDosBytes)
		
		bytesDoValor = self.memória[endereçoDaVariável : endereçoDaVariável + numBytes]
		valor = int.from_bytes(bytesDoValor, self.memória.ordemDosBytes)
		self.aumentaApontadorDeInstrução(self.memória.tamanhoDoEndereço)
		return valor
	
	def buscaOperandoIndireto(self, palavra):
		if palavra:
			numBytes = self.tamanhoDaPalavraAtual
		else:
			numBytes = self.memória.tamanhoDoEndereço
		
		ponteiroBytes = self.memória[self.apontadorDeInstrução: self.apontadorDeInstrução + self.memória.tamanhoDoEndereço]
		ponteiroEndereço = int.from_bytes(ponteiroBytes, self.memória.ordemDosBytes)
		
		bytesDaVariável = self.memória[ponteiroEndereço : ponteiroEndereço + self.memória.tamanhoDoEndereço]
		endereçoDaVariável = int.from_bytes(bytesDaVariável, self.memória.ordemDosBytes)
		
		bytesDoValor = self.memória[endereçoDaVariável : endereçoDaVariável + numBytes]
		valor = int.from_bytes(bytesDoValor, self.memória.ordemDosBytes)
		
		self.aumentaApontadorDeInstrução(self.memória.tamanhoDoEndereço)
		return valor
	
	def buscaOperandoImediato(self, palavra):
		if palavra:
			numBytes = self.tamanhoDaPalavraAtual
		else:
			numBytes = self.memória.tamanhoDoEndereço
		
		bytesDoValor = self.memória[self.apontadorDeInstrução: self.apontadorDeInstrução + numBytes]
		valor = int.from_bytes(bytesDoValor, self.memória.ordemDosBytes)
		self.aumentaApontadorDeInstrução(self.memória.tamanhoDaPalavra)
		return valor
	
	def aumentaApontadorDeInstrução(self, num):
		if callable(self.funçãoDeAtualização):
			self.funçãoDeAtualização(self.apontadorDeInstrução, self.apontadorDeInstrução+1)
		self.apontadorDeInstrução = (self.apontadorDeInstrução + num) & self.memória.endereçoMáximoDeMemória
		if callable(self.funçãoDeAtualização):
			self.funçãoDeAtualização(self.apontadorDeInstrução, self.apontadorDeInstrução+1)
		
	def adicionaFunçãoDeAtualização(self, função):	
		assert(callable(função))
		self.funçãoDeAtualização = função
		
	def removeFunçãoDeAtualização(self):
		f = self.funçãoDeAtualização
		self.funçãoDeAtualização = None
		return f	
		
	def atualizaFlags(self):
		if self.modoOitoBits:
			self.zeroFlag = (self.acumulador & self.memória.valorMáximoDaPalavraOitoBits == 0)
			self.negativeFlag = (self.acumulador & self.memória.valorMáximoDaPalavraOitoBits >= 2**(self.memória.tamanhoDaPalavraOitoBits *8 - 1))

		else:
			self.zeroFlag = (self.acumulador == 0)
			self.negativeFlag = (self.acumulador >= 2**(self.memória.tamanhoDaPalavra *8 - 1))
