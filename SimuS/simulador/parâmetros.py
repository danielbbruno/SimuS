códigoDosOperandos = {
	"NOP":		0b00000000,
	"SEP":		0b00000100,
	"REP":		0b00001000,
	"STA":		0b00010000, #"STAind":	0b00010001,
	"XGSP":		0b00010100,
	"XGAC":		0b00011000,
	"LDA":		0b00100000,	#"LDAind":	0b00100001,	#"LDAimm":	0b00100010,	
	"LDI":		0b00100010,
	"ADD":		0b00110000,	#"ADDind":	0b00110001,	#"ADDimm":	0b00110010,
	"ADC":		0b00110100,	#"ADCind":	0b00110101,	#"ADCimm":	0b00110110,
	"SUB": 		0b00111000,	#"SUBind": 	0b00111001,	#"SUBimm": 	0b00111010,
	"SBC":		0b00111100,	#"SBCind":	0b00111101,	#"SBCimm":	0b00111110,
	"OR":		0b01000000,	#"ORind":	0b01000001,	#"ORimm":	0b01000010,
	"XOR":		0b01000100,	#"XORind":	0b01000101,	#"XORimm":	0b01000110,
	"AND":		0b01010000,	#"ANDind":	0b01010001,	#"ANDimm":	0b01010010,
	"NOT":		0b01100000, 
	"SHL":		0b01110000,
	"SHR":		0b01110100,
	"SRA":		0b01111000,
	"JMP":		0b10000000,	#"JMPind":	0b10000001,
	"JN":		0b10010000,	#"JNind":	0b10010001, 
	"JP":		0b10010100,	#"JPind":	0b10010101,
	"JZ":		0b10100000,	#"JZind":	0b10100001,
	"JNZ":		0b10100100,	#"JNZind":	0b10110101,
	"JC":		0b10110000,	#"JCind":	0b10110001,
	"JNC":		0b10110100,	#"JNCind":	0b10110101,
	"IN":		0b11000000,
	"OUT":		0b11000100,
	"JSR":		0b11010000,	#"JSRind":	0b11010001,
	"RET":		0b11011000,
	"PUSH":		0b11100000,
	"POP":		0b11100100,
	"TRAP":		0b11110000, #"TRAPind": 0b11110001
	"HLT":		0b11111111,
}
operandos = set(códigoDosOperandos.keys())
diretivas = {"DW", "DB", "DS", "END", "ORG", "EQU", "STR"}

tamanhoDaPalavra = 2
tamanhoDoEndereço = 2
tamanhoDaInstrução = 1
endianness = "little"

códigosDosOperandosInvertido = {byte: nome for nome, byte in códigoDosOperandos.items()}

permiteOperandoImediato = {"LDA", "ADD", "ADC", "SUB", "SBC", "OR", "XOR", "AND"}
	
permiteOperandoIndireto = permiteOperandoImediato.union(
	{"STA", "JMP", "JN", "JP", "JZ", "JNZ", "JC", "JNC", "JSR", "TRAP"})

requerOperando = permiteOperandoIndireto.union({"LDI"})

diretivasSemEndereço = {"ORG", "END"} 
