
class Memória():
	def __init__(self, tamanhoDoEndereço, tamanhoDaPalavra, ordemDosBytes, endereçoMáximoDeMemória=False, ):
		if not endereçoMáximoDeMemória:
			self.endereçoMáximoDeMemória = 2**(tamanhoDoEndereço*8) - 1
		else:
			self.endereçoMáximoDeMemória = endereçoMáximoDeMemória
		self.ordemDosBytes = ordemDosBytes
		self.tamanhoDoEndereço = tamanhoDoEndereço
		self.dados = bytearray([0 for i in range(self.endereçoMáximoDeMemória + 1)])
		self.tamanhoDaPalavra = tamanhoDaPalavra
		self.tamanhoDaPalavraOitoBits = 1
		self.valorMáximoDaPalavra = 2**(8*tamanhoDaPalavra) - 1
		self.valorMáximoDaPalavraOitoBits = 2**(8*self.tamanhoDaPalavraOitoBits) - 1
		self.valorMáximoDoEndereço = 2**(8*tamanhoDoEndereço) - 1
		self.funçãoDeAtualização = None
		
	def __getitem__(self, chave):
		if type(chave) is int:
			return self.dados[chave % (self.endereçoMáximoDeMemória+1)]
		elif type(chave) is slice:
			if chave.start is None:
				chaveModInicio = 0
			else:
				chaveModInicio = chave.start % (self.endereçoMáximoDeMemória +1)
			if chave.stop is None:
				chaveModFim = len(self.dados)
			else:
				chaveModFim = chave.stop % (self.endereçoMáximoDeMemória +1)
			if chaveModInicio > chaveModFim:
				return self.dados[chaveModInicio: (self.endereçoMáximoDeMemória+1)] + self.dados[0:chaveModFim]
			else:
				return self.dados[chaveModInicio: chaveModFim]
		else:
			raise KeyError()

	def __setitem__(self, chave, valor):
		if type(chave) is int:
			self.dados[chave % (self.endereçoMáximoDeMemória+1)] = valor
			if callable(self.funçãoDeAtualização):
				self.funçãoDeAtualização(chave, chave)
		elif type(chave) is slice:
			if chave.start is None:
				chaveModInicio = 0
			else:
				chaveModInicio = chave.start % (self.endereçoMáximoDeMemória +1)
			if chave.stop is None:
				chaveModFim = len(self.dados)
			else:
				chaveModFim = chave.stop % (self.endereçoMáximoDeMemória +1)
			tamanho = self.endereçoMáximoDeMemória - chaveModInicio + 1
			if chaveModInicio > chaveModFim:
				self.dados[chaveModInicio:] = valor[:tamanho]
				self.dados[:chaveModFim] = valor[tamanho:]
				if callable(self.funçãoDeAtualização):
					self.funçãoDeAtualização(chaveModInicio, self.endereçoMáximoDeMemória +1)
					self.funçãoDeAtualização(0, chaveModFim)
			else:
				self.dados[chaveModInicio: chaveModFim] = valor								
				if callable(self.funçãoDeAtualização):
					self.funçãoDeAtualização(chaveModInicio, chaveModFim)
		else:
			raise KeyError()
		
	def adicionaFunçãoDeAtualização(self, função):
		assert(callable(função))
		self.funçãoDeAtualização = função
		
	def removeFunçãoDeAtualização(self):
		val = self.funçãoDeAtualização
		self.funçãoDeAtualização = None
		return val
		
	def reiniciaValores(self):		
		self.dados = bytearray([0 for i in range(self.endereçoMáximoDeMemória + 1)])

