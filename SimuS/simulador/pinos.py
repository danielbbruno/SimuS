

from functools import wraps
from SimuS.simulador.exceções import ExceçãoTRAPIndisponível
try:
    import RPi.GPIO as gpio
except ModuleNotFoundError:
    gpio = None


class Pinos:
    """Classe responsável por manter o estado dos pinos do Raspberry Pi."""
    ENTRADA = 0
    SAÍDA = 1
    PWM = 3
    LIBERADO = 4    
        
    def __init__(self):
        if gpio is None:
            self.funcionalidadesPI = False
        else:
            self.funcionalidadesPI = True
            self.reseta()
        
    def checaEstado(função):
        """Decorador que checa se a biblioteca de pinos do Raspberry foi carregada, e caso não tenha sido é lançada exceção."""
        @wraps(função)
        def wrapper(self, *args):
            if self.funcionalidadesPI:
                try:
                    função(self, *args)
                except RuntimeError:
                    raise ExceçãoTRAPIndisponível("Sem acesso a /dev/mem.")
            else:
                raise ExceçãoTRAPIndisponível()
        return wrapper
    
    @checaEstado    
    def reseta(self,):
        gpio.setmode(gpio.BCM)
        self.dicionárioModos = {
            self.ENTRADA:   gpio.IN,
            self.SAÍDA:     gpio.OUT,
            self.PWM:       gpio.PWM,
            self.LIBERADO:  None,           
        }
        self.pinos = {i:self.LIBERADO for i in range(27)}
        self.objetosPWM = {}
    
    @checaEstado
    def configura(self, pino, modo):
        assert (modo in (self.ENTRADA, self.SAÍDA, self.PWM))
        assert(pino in self.pinos.keys())
        assert(self.pinos[pino] == self.LIBERADO)
        
        self.pinos[pino] = modo
        
        gpioModo = self.dicionárioModos[modo]
        if modo == self.PWM:
            self.objetosPWM[pino] = gpio.PWM(pino, 600000)
        else:            
            gpio.setup(pino, gpioModo)
            
    @checaEstado            
    def pegaValor(self, pino):
        if self.pinos[pino] not in (self.PWM, self.SAÍDA):
            raise Exception()
        valor = gpio.input(pino)
        return bool(valor)
    
    @checaEstado    
    def escreveValor(self, pino, valor):
        if self.pinos[pino] != self.SAÍDA:
            raise Exception()
        gpio.output(pino, bool(valor))
        
    @checaEstado       
    def configuraPullUpPullDown(self, pino, valor):
        if self.pinos[pino] not in (self.ENTRADA, self.SAÍDA):
            raise Exception()
        
        modo = self.pinos[pino]
        gpioModo = self.dicionárioModos[modo]
        if valor is None:
            gpio.setup(pino, gpioModo, pull_up_down=gpio.PUD_OFF)           
        elif valor:
            gpio.setup(pino, gpioModo, pull_up_down=gpio.PUD_UP)
        else:            
            gpio.setup(pino, gpioModo, pull_up_down=gpio.PUD_DOWN)
            
    @checaEstado            
    def configuraDC(self, pino, valor):
        assert (pino in self.objetosPWM.keys())
        assert((valor >= 0) and (valor <= 1023))
        obj = self.objetosPWM[pino]
        obj.ChangeDutyCycle((100*valor)/1024)
        
    @checaEstado
    def limpaEstado(self):
        gpio.cleanup()
        self.reseta()
        
        
