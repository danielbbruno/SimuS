
import sys, os

from PyQt5.QtWidgets import QApplication
from PyQt5 import QtCore

import pkg_resources
import webbrowser


from SimuS.interfaceDeUsuário.janelaPrincipal import JanelaPrincipal
from SimuS.controlador.controleDeCompilação import ControleDeCompilação
from SimuS.controlador.controleDeSimulação import ControleDeSimulação
from SimuS.controlador.configurações import Configurações

URL_SOBRE_O_PROGRAMA = 'https://gitlab.com/danielbbruno/SimuS/-/wikis/Sobre-o-SimuS'
MANUAL_SOBRE_O_PROGRAMA = 'https://gitlab.com/danielbbruno/SimuS/-/wikis/Manual'

class ClassePrincipal:
	def inicia(self):
		if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
			QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

		if hasattr(QtCore.Qt, 'AA_UseHighDpiPixmaps'):
			QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)

		self.app = QApplication(sys.argv)
		self.app.setPalette(QApplication.style().standardPalette())
		nomeDoArquivo = pkg_resources.resource_filename('SimuS', 'interfaceDeUsuário/folhasDeEstilo.qss')
		with open(nomeDoArquivo, "r") as fh:
			self.app.setStyleSheet(fh.read())
			
		self.preparaTradutor()
			
		áreaDeTransferência = self.app.clipboard()
		self.janela = JanelaPrincipal(self, áreaDeTransferência)
		self.cCom = ControleDeCompilação(self)
		self.cSim = ControleDeSimulação(self, self.app)
		self.conf = Configurações(self)
		self.cSim.memória.adicionaFunçãoDeAtualização(self.funçãoDeAtualização)

		self.janela.acaoCompila.triggered.connect(lambda: self.cCom.compila())
		
		self.janela.widgetControleDeExecução.simulaButton.clicked.connect(lambda: self.cSim.passo(self.janela.widgetControleDeExecução.velocidadeSlider))
		self.janela.widgetControleDeExecução.pausaButton.clicked.connect(lambda: self.cSim.pausa())		
		self.janela.widgetControleDeExecução.paraButton.clicked.connect(lambda: self.cSim.para())
		self.janela.widgetControleDeExecução.breakpointButton.clicked.connect(lambda: self.cSim.avança(self.janela.widgetControleDeExecução.velocidadeSlider))

		self.janela.actionSobre.triggered.connect(self.abrirSobre)
		self.janela.actionManual.triggered.connect(self.abrirManual)		
		
		self.janela.iniciaMonitorDaPilha()
		self.janela.iniciaMonitorDeDados()
		self.janela.show()		
		sys.exit(self.app.exec_())
		
	def funçãoDeAtualização(self, inicio, fim):
		self.janela.atualizaMonitorDeDados(inicio, fim)
		self.janela.atualizaMonitorDaPilha(inicio, fim)

	def preparaTradutor(self,):
		from PyQt5.QtCore import QLocale, QTranslator, QLibraryInfo
		tradutor = QTranslator()
		arquivo = 'qt_' + QLocale.system().name()
		caminho = QLibraryInfo.location(QLibraryInfo.TranslationsPath)
		if tradutor.load(arquivo, caminho):
			self.app.installTranslator(tradutor)

	def abrirSobre(self,):
		webbrowser.open(URL_SOBRE_O_PROGRAMA)
	
	def abrirManual(self,):
		webbrowser.open(MANUAL_SOBRE_O_PROGRAMA)
