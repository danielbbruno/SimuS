

from SimuS.compilador import AnalisadorLéxico, AnalisadorSintático
from SimuS.compilador.exceções import ExceçãoDeCompilação

class ControleDeCompilação:
	def __init__(self, cp):
		self.cp = cp
		self.alternaAba = False
		
	def compila(self):
		janela = self.cp.janela
		máquina = self.cp.cSim.máquina
		memória = máquina.memória
		
		funcAtualização = memória.removeFunçãoDeAtualização()
		janela.escondeAvisosAntigos()
		self.texto = janela.pegaTexto()
		al = AnalisadorLéxico(self.texto)
		try:
			al.iniciaAnálise()
			self.textoSimplificado = al.geraTextoSimplificado()
		except ExceçãoDeCompilação as e:
			janela.escreveAviso(e.mensagemParaConsole)
			janela.escreveAviso("A compilação falhou.")		
		else:	
			tokens = al.símbolosProcessados
			asin = AnalisadorSintático(tokens, memória)
			try:
				janela.arquivaMensagens()
				asin.iniciaAnálise()
			except ExceçãoDeCompilação as e:
				janela.escreveAviso(e.mensagemParaConsole)
				janela.escreveAviso("A compilação falhou.")
			else:
				janela.atualizaParâmetros(asin.endereçosDosOperadores, asin.endereçosDasAlocações, asin.definiçõesDeRótulos)
				self.cp.cSim.estado = "Parado"
				máquina.zeraEstado()
				máquina.apontadorDeInstrução = asin.end
				máquina.associaConsole(janela)
				self.cp.cSim.zeraBreakpoints()
				janela.esvaziaBuffer()
				janela.preencheMonitorDeDados()
				janela.widgetControleDeExecução.atualizaEstado()
				janela.widgetEstadoDaMáquina.atualizaValores(máquina)
				janela.atualizaMonitorDaPilha(0, -1)
				janela.escreveAviso("Programa compilado com sucesso.")
				if self.alternaAba:
					janela.areaSuperior.setCurrentWidget(janela.tabMemoria)
		memória.adicionaFunçãoDeAtualização(funcAtualização)