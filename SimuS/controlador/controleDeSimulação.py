
from SimuS.simulador import Máquina, Memória
import time
import copy
from SimuS.simulador.parâmetros import tamanhoDoEndereço, tamanhoDaPalavra, endianness
from SimuS.simulador.exceções import ExceçãoDeOperandoInválido, ExceçãoTRAPIndisponível, ExceçãoTRAPInválido, ExceçãoDeOpcodeInválido, ExceçãoTRAPIndisponível

class ControleDeSimulação:
	taxa = 0.25
	
	def __init__(self, cp, app):
		self.memória = Memória(tamanhoDoEndereço=tamanhoDoEndereço,
							 tamanhoDaPalavra=tamanhoDaPalavra,
							 ordemDosBytes=endianness)
		self.máquina = Máquina(self.memória, cp)
		self.processaEventos = app.processEvents
		self.janela = cp.janela
		self.estado = "Parado"
		self.máquina.associaConsole(self.janela)
		self.breakpoints = []
		
	def ativaBreakpoint(self):
		itens = self.janela.tabelaInstrucoes.selectedItems()
		linha = itens[0].row()
		for chave, valor in self.janela.itensDeInstruções.items():
			if valor == linha:
				end = chave
		if end in self.breakpoints:
			self.breakpoints.remove(end)
			self.janela.desmarcaBreakpoint(linha, end)
		else:
			self.breakpoints.append(end)
			self.atualizaInterface()
		
	def zeraBreakpoints(self):
		self.breakpoints = []
		
	def atualizaInterface(self):
		self.janela.widgetEstadoDaMáquina.atualizaValores(self.máquina)
		self.janela.atualizaMonitorDaPilha(self.máquina.apontadorDeInstrução)
		self.janela.atualizaApontadorDePrograma()
	
	def armazenaEstado(self):
		console = self.máquina.desassociaConsole()
		
		f1 = self.máquina.memória.removeFunçãoDeAtualização()
		f2 = self.máquina.removeFunçãoDeAtualização()
		self.máquinaInicial = copy.deepcopy(self.máquina)
		
		self.máquina.associaConsole(console)
		self.máquinaInicial.associaConsole(console)
		if callable(f1):
			self.máquina.memória.adicionaFunçãoDeAtualização(f1)
			self.máquinaInicial.memória.adicionaFunçãoDeAtualização(f1)
		if callable(f2):
			self.máquina.adicionaFunçãoDeAtualização(f2)
			self.máquinaInicial.adicionaFunçãoDeAtualização(f2)
			
	def passo(self, velocidadeSlider):
		if self.estado == "Parado":
			self.armazenaEstado()
		self.estado = "Executando"
		self.janela.widgetControleDeExecução.atualizaEstado()

		velocidade = None
		res = ""
		tempoAntigo = time.time()
		while res != "HLT" and self.estado == "Executando":
			tempoAtual = time.time()
			if velocidadeSlider.value() != velocidade:
				velocidade = velocidadeSlider.value()
				if velocidade == velocidadeSlider.maximum():
					intervalo = 0
				elif velocidade == velocidadeSlider.minimum():
					res = self.passoDaMáquina()
					self.atualizaInterface()
					self.processaEventos()
					self.estado = "Pausado"
					self.janela.widgetControleDeExecução.atualizaEstado()
					return
				else:
					intervalo = (1.0/velocidade)/self.taxa
		
			if tempoAtual - tempoAntigo > intervalo:
				res = self.passoDaMáquina()
				self.atualizaInterface()
				self.processaEventos()
				tempoAntigo = tempoAtual
			else:
				self.processaEventos()
		if res == "HLT":
			self.estado = "Pausado"
			self.janela.widgetControleDeExecução.atualizaEstado()
		
	def pausa(self):
		if self.estado == "Executando":
			self.estado = "Pausado"			
			self.janela.widgetControleDeExecução.atualizaEstado()
	
	def para(self):
		self.estado = "Parado"
		try:
			self.máquina.limpaEstado()
		except ExceçãoTRAPIndisponível:
			pass
			
		self.janela.esvaziaBuffer()
		self.janela.widgetControleDeExecução.atualizaEstado()
		self.máquina = self.máquinaInicial
		self.memória = self.máquina.memória
		self.memória.adicionaFunçãoDeAtualização(self.janela.atualizaMonitorDeDados)
		self.atualizaInterface()
		self.janela.preencheMonitorDeDados()
	
	def avança(self, velocidadeSlider):
		if self.estado == "Parado":
			self.armazenaEstado()
		self.estado = "Executando"
		self.janela.widgetControleDeExecução.atualizaEstado()
		res = self.passoDaMáquina()
		self.atualizaInterface()
		self.processaEventos()

		velocidade = None
		tempoAntigo = time.time()
		while (res != "HLT" and 
			self.estado == "Executando" and
			not (self.máquina.apontadorDeInstrução in self.breakpoints)):
			tempoAtual = time.time()
			if velocidadeSlider.value() != velocidade:
				velocidade = velocidadeSlider.value()
				if velocidade == velocidadeSlider.maximum():
					intervalo = 0
				elif velocidade == velocidadeSlider.minimum():
					self.estado = "Pausado"
					self.janela.widgetControleDeExecução.atualizaEstado()
					return
				else:
					intervalo = (1.0/velocidade)/self.taxa
		
			if tempoAtual - tempoAntigo > intervalo:
				res = self.passoDaMáquina()
				self.atualizaInterface()
				self.processaEventos()
				tempoAntigo = tempoAtual
			else:
				self.processaEventos()
		if res == "HLT" or (self.máquina.apontadorDeInstrução in self.breakpoints):
			self.estado = "Pausado"
			self.janela.widgetControleDeExecução.atualizaEstado()
				
	def passoDaMáquina(self):
		val = self.máquina.apontadorDeInstrução 
		try:
			resultado = self.máquina.passo()
		except (ExceçãoDeOpcodeInválido, ExceçãoDeOperandoInválido, ExceçãoTRAPInválido, ExceçãoTRAPIndisponível) as e:
			self.estado = "Pausado"			
			self.máquina.apontadorDeInstrução = val
			self.janela.escreveAviso(e.mensagem)
		else:
			return resultado
			