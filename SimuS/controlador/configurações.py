from PyQt5.QtCore import QResource
from PyQt5.QtWidgets import QStyleFactory
from PyQt5.QtGui import QFontMetrics, QFont, QColor
import configparser as conf   
import pkg_resources
import math

class Configurações(conf.ConfigParser):
    def __init__(self, cp):
        self.cp = cp
        self.janela = self.cp.janela
        
        self.__íconesDeAçãoJanela = {
            "actionAbrirArquivo":   ("document-open", "folder"),
            "acaoEscondeAvisos":    ("utilities-terminal", "terminal"),
            "actionPreferencias":   ("preferences-other", "settings"),
            "acaoCompila":          ("object", "zap"),
            "actionSair":           ("application-exit", "x-square"),
            "actionNovo_arquivo":   ("document-new", "file"),
            "actionSobre":          ("help-about", "info"),
            "actionManual":         ("help-contents", "help-circle"),
            "actionSalvar":         ("document-save", "save"),
            "actionCopiar":         ("edit-copy", "copy"),
            "actionCortar":         ("edit-cut", "scissors"),
            "actionColar":          ("edit-paste", "clipboard"),
            "actionDesfazer":       ("edit-undo", "corner-up-left"),
            "actionRefazer":        ("edit-redo", "corner-up-right"),
            "actionSalvarComo":     ("document-save-as", None),
        }
        
        self.__íconesDeAçãoWidgetDeExecução = {
            "simulaButton": ("media-playback-start", "play"),
            "pausaButton": ("media-playback-pause", "pause"),
            "breakpointButton": ("media-seek-forward", "fast-forward"),
            "paraButton": ("media-playback-stop", "square"),
        }

        conf.ConfigParser.__init__(self)
        import os
        if os.name == "nt":
            caminho = os.getenv('APPDATA')
            caminho = os.path.join(caminho, "simus16")
        else:
            caminho = os.path.expanduser("~/.config/simus16/")
        os.makedirs(caminho, exist_ok=True)
        self.nomeDoArquivo = os.path.join(caminho, "configurações.cfg")
        try:
            self.carregaDoArquivo()
        except:
            self.carregaConfiguraçõesPadrão()
        self.aplicaConfigurações(False)
        
    def inicializaÍcones(self):
        nomeDoArquivoDeÍcones = pkg_resources.resource_filename('SimuS', 'interfaceDeUsuário/íconesFeatherBin.rcc')
        ícones = QResource()
        if not ícones.registerResource(nomeDoArquivoDeÍcones):
            print("Não foi possível carregar o arquivo de ícones em '" + str(nomeDoArquivoDeÍcones) + "'")
            return None
        else:
            return ícones
        
    def possuiÍconesPadrão(self):
        from PyQt5.QtGui import QIcon
        for  nomeDaAção, nomeDoÍcone in self.__íconesDeAçãoJanela.items():
            if nomeDoÍcone is not None:
                if not QIcon.hasThemeIcon(nomeDoÍcone[0]):
                    return False
        for  nomeDaAção, nomeDoÍcone in self.__íconesDeAçãoWidgetDeExecução.items():
            if nomeDoÍcone is not None:
                if not QIcon.hasThemeIcon(nomeDoÍcone[0]):
                    return False
        return True
    
    def carregaÍcones(self, alternativo = False):
        from PyQt5.QtGui import QIcon
        
        if alternativo:
            self.ícones = self.inicializaÍcones()
        
        for  nomeDaAção, nomeDoÍcone in self.__íconesDeAçãoJanela.items():
            nomeDoÍcone = nomeDoÍcone[1] if alternativo else nomeDoÍcone[0]
            ação = getattr(self.cp.janela, nomeDaAção)
            if nomeDoÍcone is None:
                ícone = QIcon()
            else:
                ícone = QIcon(":/ícones/" + nomeDoÍcone + ".svg.png") if alternativo else QIcon.fromTheme(nomeDoÍcone)
            ação.setIcon(ícone)
            
        for  nomeDaAção, nomeDoÍcone in self.__íconesDeAçãoWidgetDeExecução.items():
            nomeDoÍcone = nomeDoÍcone[1] if alternativo else nomeDoÍcone[0]
            ação = getattr(self.cp.janela.widgetControleDeExecução, nomeDaAção)
            if nomeDoÍcone is None:
                ícone = QIcon()
            else:
                ícone = QIcon(":/ícones/" + nomeDoÍcone + ".svg.png") if alternativo else QIcon.fromTheme(nomeDoÍcone)
            ação.setIcon(ícone)

    def carregaConfiguraçõesPadrão(self):
        íconesAlternativos = not self.possuiÍconesPadrão()
        
        self["editor"] = {    "fonte": "Monospace",
                                "tamanho da fonte": "12",
                                "largura da tabulação": "4",
                                "texto rgb":"255,153,0",
                                "operador rgb":"0,0,180",
                                "numeral rgb":"180,0,0",
                                "comentário rgb":"180,180,180",
                                "rótulo rgb":"0,180,0",
                                "diretiva rgb":"120,120,255",
                            }
        
        self["geral"] = {     "ícones alternativos": "1" if íconesAlternativos else "0",
                                "alternar aba": "1",
                                "acompanha instrução": "1",
                                "acompanha pilha": "1",        
                           }
        
        self["console"] = {   "máximo de mensagens": "50",
                                "alertar ao executar dados": "1",
                                "limpar avisos": "0",
                                "fonte": QFont().defaultFamily(),
                                "tamanho da fonte": "11.0"                                 
                            }
        
    def carregaDoArquivo(self):
        with open(self.nomeDoArquivo, "r") as arq:
            self.read_file(arq)
    
    def salvaConfigurações(self):
        with open(self.nomeDoArquivo, "w") as arq:
            self.write(arq)
                
    def aplicaConfigurações(self, salvar=True):
        j = self.cp.janela
        
        fonte = QFont()
        fonte.setFamily(self["editor"]["fonte"])
        fonte.setPointSize(math.ceil(float(self["editor"]["tamanho da fonte"])))
        j.editorDeCodigo.setFont(fonte)
             
        medida = QFontMetrics(fonte)   
        tamanho = medida.width(' ')
        j.editorDeCodigo.setTabStopWidth(tamanho * int(self["editor"]["largura da tabulação"]))
        
        self.carregaÍcones(int(self["geral"]["ícones alternativos"]))
        
        fonte = QFont()
        fonte.setFamily(self["console"]["fonte"])
        fonte.setPointSize(math.ceil(float(self["console"]["tamanho da fonte"])))
        j.areaDeAvisos.setFont(fonte)
        
        self.cp.cCom.alternaAba = bool(int(self["geral"]["alternar aba"]))
        
        j.destacador.corComentário = QColor(*self.cores["comentário"])
        j.destacador.corNumeral = QColor(*self.cores["numeral"])
        j.destacador.corOperador = QColor(*self.cores["operador"])
        j.destacador.corTexto = QColor(*self.cores["texto"])
        j.destacador.corRótulo = QColor(*self.cores["rótulo"])
        j.destacador.corDiretiva = QColor(*self.cores["diretiva"])
        
        j.destacador.defineFormatação()
        j.destacador.defineRegras()
        
        j.máximoDeMensagens = int(self["console"]["máximo de mensagens"])
        j.limparAvisosAoMontar = bool(int(self["console"]["limpar avisos"]))
        
        if salvar:
            self.salvaConfigurações()
    
    @property  
    def cores(self):
        cores = {}
        cores["operador"] =  tuple(int(i) for i in self["editor"]["operador rgb"].split(","))
        cores["numeral"] =  tuple(int(i) for i in self["editor"]["numeral rgb"].split(","))
        cores["comentário"] = tuple(int(i) for i in self["editor"]["comentário rgb"].split(","))
        cores["texto"] =  tuple(int(i) for i in self["editor"]["texto rgb"].split(","))
        cores["rótulo"] =  tuple(int(i) for i in self["editor"]["rótulo rgb"].split(","))
        cores["diretiva"] =  tuple(int(i) for i in self["editor"]["diretiva rgb"].split(","))
        return cores
    
    @cores.setter
    def cores(self, valor):
        """Propriedade para a alteração das cores. Note que o dicionário inteiro, contendo todas as cores deve ser utilizado como valor de entrada."""
        self["editor"]["operador rgb"] = ",".join(str(i) for i in valor["operador"])
        self["editor"]["numeral rgb"] = ",".join(str(i) for i in valor["numeral"])
        self["editor"]["comentário rgb"] = ",".join(str(i) for i in valor["comentário"])
        self["editor"]["texto rgb"] = ",".join(str(i) for i in valor["texto"])
        self["editor"]["rótulo rgb"] = ",".join(str(i) for i in valor["rótulo"])
        self["editor"]["diretiva rgb"] = ",".join(str(i) for i in valor["diretiva"])
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                