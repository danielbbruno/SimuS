
from SimuS.compilador.analisadorLéxicoBase import AnalisadorLéxicoBase, Símbolo

class AnalisadorLéxico(AnalisadorLéxicoBase):
	def __init__(self, texto):
		self.contadorDeSímbolos = 0
		self.contadorDeLinhas = 0
		self.operadores = [
			(r"[Nn][Oo][Pp]", self.retornaSímbolo, ("Operador",)),
			(r"[Ss][Ee][Pp]", self.retornaSímbolo, ("Operador",)),
			(r"[Rr][Ee][Pp]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ss][Tt][Aa]", self.retornaSímbolo, ("Operador",)),
			(r"[Xx][Gg][Ss][Pp]", self.retornaSímbolo, ("Operador",)),
			(r"[Xx][Gg][Aa][Cc]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ll][Dd][Aa]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Aa][Dd][Dd]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Aa][Dd][Cc]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ss][Uu][Bb]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ss][Bb][Cc]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Oo][Rr]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Xx][Oo][Rr]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Aa][Nn][Dd]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Nn][Oo][Tt]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ss][Hh][Ll]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Ss][Hh][Rr]", self.retornaSímbolo, ("Operador",)),			   
			(r"[Ss][Rr][Aa]", self.retornaSímbolo, ("Operador",)),		   
			(r"[Jj][Mm][Pp]", self.retornaSímbolo, ("Operador",)),	   
			(r"[Jj][Nn]", self.retornaSímbolo, ("Operador",)),	   
			(r"[Jj][Pp]", self.retornaSímbolo, ("Operador",)),	   
			(r"[Jj][Zz]", self.retornaSímbolo, ("Operador",)),  
			(r"[Jj][Nn][Zz]", self.retornaSímbolo, ("Operador",)),  
			(r"[Jj][Cc]", self.retornaSímbolo, ("Operador",)),  
			(r"[Jj][Nn][Cc]", self.retornaSímbolo, ("Operador",)), 
			(r"[Jj][Ss][Rr]", self.retornaSímbolo, ("Operador",)),  
			(r"[Rr][Ee][Tt]", self.retornaSímbolo, ("Operador",)),  
			(r"[Pp][Oo][Pp]", self.retornaSímbolo, ("Operador",)),  
			(r"[Pp][Uu][Ss][Hh]", self.retornaSímbolo, ("Operador",)),  
			(r"[Tt][Rr][Aa][Pp]", self.retornaSímbolo, ("Operador",)),
			(r"[Hh][Ll][Tt]", self.retornaSímbolo, ("Operador",)),
			(r"[Ll][Dd][Ii]", self.retornaSímbolo, ("Operador",)),
		]
		self.diretivas = [
			(r"[Dd][Ss]", self.retornaSímbolo, ("Diretiva",)),
			(r"[Dd][Ww]", self.retornaSímbolo, ("Diretiva",)), 
			(r"[Dd][Bb]", self.retornaSímbolo, ("Diretiva",)),  
			(r"[Ee][Qq][Uu]", self.retornaSímbolo, ("Diretiva",)),
			(r"[Ee][Nn][Dd]", self.retornaSímbolo, ("Diretiva",)),
			(r"[Oo][Rr][Gg]", self.retornaSímbolo, ("Diretiva",)),
			(r"[Ss][Tt][Rr]", self.retornaSímbolo, ("Diretiva",)),
		]			
			
		self.numeros = [
			(r"(?![@#])[0-9]+", self.retornaNumeral, (None,)),
			(r"(?![@#])[0][b][0-1]+", self.retornaNumeral, (None,)),
			(r"(?![@#])[0][x][0-9a-fA-F]+", self.retornaNumeral, (None,)),
			(r"[#@][0-9]+", self.retornaTipoDeOperando, ("Numeral",)),
			(r"[#@][0][b][0-1]+", self.retornaTipoDeOperando, ("Numeral",)),
			(r"[#@][0][x][0-9a-fA-F]+", self.retornaTipoDeOperando, ("Numeral",)),
		]
		
		self.cadeiasDeCaracteres = [
			(r"'[^']*'", self.retornaString, (None,)),
			(r'"[^"]*"', self.retornaString, (None,)),
		]
			
		self.rotulos = [			
			(r"[A-Za-zÀ-ÿ_]+[0-9]*:", self.retornaSímboloDefiniçãoDeRótulo, (None,)),
			(r"[A-Za-zÀ-ÿ_]+[0-9]*", self.retornaSímbolo, ("Rótulo",)),			
			(r"[#][A-Za-zÀ-ÿ_]+[0-9]*", self.retornaTipoDeOperando, ("Rótulo",)),
			(r"[@][A-Za-zÀ-ÿ_]+[0-9]*", self.retornaTipoDeOperando, ("Rótulo",)),			
		]
		
		self.separadores = [
			(r",", self.retornaSeparador, (",",)),
			(r"\+", self.retornaSeparador, ("+",)),
			(r"\n", self.retornaSeparador, ("NovaLinha",)),
			(r"[\t ]+", None, (None,)),
			(r"\Z", self.retornaSeparador, ("EOF",))
		]
		self.comentarios = [
			(r";.*(?=\n)", None, None),
			(r";.*\Z", None, None),
		]
		
		self.vocabulário = self.operadores + self.diretivas + self.cadeiasDeCaracteres + self.numeros + self.rotulos + self.comentarios + self.separadores
		AnalisadorLéxicoBase.__init__(self, texto)
		
	def geraSímboloComLinha(self, tipo, valor=None, outrosArgs= None):
		if tipo == "NovaLinha":
			self.contadorDeLinhas += 1
		self.contadorDeSímbolos += 1
		return Símbolo(tipo, valor, outrosArgs, self.contadorDeLinhas, self.contadorDeSímbolos)
		
	def retornaSímbolo(self, palavra, args=None):
		if not args:
			return Símbolo(tipo=palavra)
		else:
			if type(palavra) == str:
				palavra = palavra.upper()
			return (self.geraSímboloComLinha(tipo=args, valor=palavra),)
		
	def retornaNumeral(self, palavra, args=None):
		if palavra[:2] == "0b":
			valor = int(palavra, 2)
		elif palavra[:2] == "0x":
			valor = int(palavra, 16)
		else:
			valor = int(palavra)
		return (self.geraSímboloComLinha(tipo="Numeral", valor=valor, outrosArgs=palavra),)
	
	def retornaString(self, palavra, args=None):
		valor = palavra[1:-1]
		return (self.geraSímboloComLinha(tipo="CadeiaDeCaracteres", valor=valor), )
		
	def retornaSímboloDefiniçãoDeRótulo(self, palavra, args):
		palavra = palavra[:-1]
		palavra = palavra.upper()
		return (self.geraSímboloComLinha("DefiniçãoDeRótulo", palavra),)
	
	def retornaTipoDeOperando(self, palavra, args):
		if args == "Numeral":
			return self.geraSímboloComLinha(tipo=palavra[0]), self.retornaNumeral(palavra[1:])[0]
		else:
			return self.geraSímboloComLinha(tipo=palavra[0]), self.geraSímboloComLinha(tipo="Rótulo", valor=palavra[1:].upper())
	
	def retornaSeparador(self, palavra, args):
		if args == "EOF": 
			self.estado = "finalizado"
		return (self.geraSímboloComLinha(tipo = args),)
		