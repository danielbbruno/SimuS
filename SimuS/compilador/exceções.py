
class ExceçãoDeCompilação(Exception):
	def geraMensagemParaConsole(self):
		if self.linha is not None:
			self.mensagemParaConsole = '<a href="' + str(self.linha) + '">Linha ' + str(self.linha + 1) + ":</a> " + self.mensagem
		else:
			self.mensagemParaConsole = self.mensagem
			
class ExceçãoSímboloInesperado(ExceçãoDeCompilação):
	def __init__(self, linha, posição, mensagem, recebido):
		self.linha = linha
		self.posição = posição
		if recebido is None:
			self.mensagem = mensagem
		else:
			self.mensagem = mensagem % (str(recebido))
		self.recebido = recebido
		super(ExceçãoSímboloInesperado, self).__init__(self.mensagem)		
		self.geraMensagemParaConsole()

class ExceçãoDefiniçãoDeRótuloRepetida(ExceçãoDeCompilação):
	def __init__(self, linhaOriginal, linha, valor):
		self.linhaOriginal = linhaOriginal
		self.linha = linha
		self.valor = valor
		self.mensagem = "o rótulo '" + str(valor) + "' já foi definido na linha "+ str(linhaOriginal) + "."
		self.mensagemParaConsole = '<a href="' + str(self.linha) + '">Linha ' + str(self.linha + 1) + ":</a> o rótulo '" + str(valor) + "' já foi definido na <a href='"+ str(linhaOriginal)+ "'>linha "+ str(linhaOriginal+1) + "</a>."

class ExceçãoRótuloSemDefinição(ExceçãoDeCompilação):
	def __init__(self, linha, pos, valor):
		self.linha = linha
		self.pos = pos
		self.valor = valor
		self.mensagem = "o rótulo '" + str(valor) + "' foi usado mas não foi definido."
		super(ExceçãoRótuloSemDefinição,self).__init__(self.mensagem)		
		self.geraMensagemParaConsole()
		
class ExceçãoSímboloInválido(ExceçãoDeCompilação):
	def __init__(self, linha, pos, tipo=None, valor=None):
		self.linha = linha
		self.pos = pos
		self.valor = valor
		self.tipo = tipo
		self.mensagem = ""
		if linha is None:
			self.mensagem = "tipo inválido para um símbolo: '" + str(self.tipo) + "'."
		elif tipo is None:
			self.mensagem = "rótulo inválido."	
		elif valor is None:
			self.mensagem = "tipo inválido para um símbolo: '" + str(self.tipo) + "'."
		else:
			self.mensagem = str("símbolos do tipo '" + str(self.tipo) + "' não podem ter valor '" +
			str(self.valor) + "'.")
		super(ExceçãoSímboloInválido, self).__init__(self.mensagem)		
		self.geraMensagemParaConsole()
			
class ExceçãoUsoIncorretoDaMemória(ExceçãoDeCompilação):
	def __init__(self, mensagem, posiçãoInicial =None, posiçãoFinal = None):
		if posiçãoFinal is not None and posiçãoInicial is not None:
			self.mensagem = mensagem % (posiçãoInicial, posiçãoFinal)
		else:
			self.mensagem = mensagem
		super(ExceçãoUsoIncorretoDaMemória, self).__init__(self.mensagem)
		self.posiçãoInicial = posiçãoInicial
		self.posiçãoFinal = posiçãoFinal
		
		self.mensagemParaConsole = self.mensagem
		
		
class ExceçãoOperandoInválido(ExceçãoDeCompilação):
	def __init__(self, mensagem, linha):
		assert(type(linha) is int)
		self.mensagem = mensagem
		self.linha = linha
		super(ExceçãoOperandoInválido, self).__init__(self.mensagem)		
		
		self.geraMensagemParaConsole()
		
class ExceçãoValorForaDosLimites(ExceçãoDeCompilação):
	def __init__(self, valor, linha):
		assert(type(linha) is int)
		self.linha = linha
		self.mensagem = "valor " + str(valor) + " não pode ser representado em dois bytes."
		super(ExceçãoValorForaDosLimites, self).__init__(self.mensagem)		
		self.geraMensagemParaConsole()
		