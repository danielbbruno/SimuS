from SimuS.simulador import parâmetros
from math import log, ceil
from SimuS.compilador.exceções import *
from SimuS.compilador.analisadorLéxicoBase import Símbolo

class AnalisadorSintático:
	def __init__(self, símbolos, memória):
		self.símbolos = símbolos
		self.memória= memória
		self.__linha = 0		
		self.__trechos = {}
		self.__posiçãoNoTexto = 0
		self.__ultimoOrg = 0
		self.tamanhoDoEndereço = memória.tamanhoDoEndereço
		self.tamanhoDaPalavra = memória.tamanhoDaPalavra
		self.ordemDosBytes = memória.ordemDosBytes
		self.valorMáximoDaPalavra = memória.valorMáximoDaPalavra
		self.parâmetroDiretivaEnd = (None, None, None) #parâmetro, pos, linha
		self.endereçosDosOperadores = []
		self.endereçosDosOperandos = []
		self.endereçosDosOperandosImediatos = []
		self.endereçosDasAlocações =[]
	
	def iniciaAnálise(self):
		self.memória.reiniciaValores()
		self.definiçõesDeRótulos = {}
		self.rótulosUsados = {}
		
		esperaLinha = False
		while self.__posiçãoNoTexto < len(self.símbolos):
			simb = self.__lêSímbolo()
			tipo = simb.tipo
			sval = simb.valor
			
			if tipo == "NovaLinha":
				self.__linha += 1				
				esperaLinha = False
				
			elif tipo == "Operador":				
				if esperaLinha == True:
					raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto -1, 
												"o %s deve ser definido em uma linha sem outros operadores ou diretivas.",
												 simb)
				esperaLinha = True
				opcode = parâmetros.códigoDosOperandos[sval.upper()]
				permiteOperandoImediato = sval in parâmetros.permiteOperandoImediato
				permiteOperandoIndireto = sval in parâmetros.permiteOperandoIndireto
				tamanhoDoOperando = self.tamanhoDoEndereço
				requerOperando = sval in parâmetros.requerOperando
				
				if requerOperando:
					próximoSímbolo = self.__lêSímbolo()
					operTipo = None
					
					if próximoSímbolo.tipo == "@":
						if not permiteOperandoIndireto:
							raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto -1, 
												"o %s não aceita operando indireto.",
												 simb)
						opcode += 1
						tamanhoDoOperando = self.tamanhoDoEndereço
						próximoSímbolo = self.__lêSímbolo()
						self.registraOperador(self.tamanhoDoEndereço)
						operTipo = "@"
						
					elif próximoSímbolo.tipo == "#":
						if not permiteOperandoImediato:
							raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto -1, 
												"o %s não aceita operando imediato.",
												 simb)
						opcode += 2
						tamanhoDoOperando = self.tamanhoDaPalavra
						próximoSímbolo = self.__lêSímbolo()
						self.registraOperador(self.tamanhoDaPalavra)
						operTipo = "#"
						
					elif próximoSímbolo.tipo in ("Rótulo", "Numeral"):
						self.registraOperador(self.tamanhoDoEndereço)
						
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
													 self.__posiçãoNoTexto - 2,
													 "Era esperado um operando(ou um dos símbolos '#', '@'), mas foi recebido %s.",
													 próximoSímbolo)
					self.__atualizaTrechos(opcode)
					if próximoSímbolo.tipo == "Rótulo":
						res = self.checaAdiante([Símbolo("+"), Símbolo("Numeral", 0)])
						if res:
							deslocamento = res[1].valor
						else:
							deslocamento = 0
						if operTipo == "#":
							self.registraOperandoImediato(próximoSímbolo.valor.upper())
						else:
							self.registraOperando(próximoSímbolo.valor.upper())
						if próximoSímbolo.valor.upper() in self.rótulosUsados:
							self.rótulosUsados[próximoSímbolo.valor.upper()].append((len(self.__trechos[self.__ultimoOrg]), self.__linha, tamanhoDoOperando, self.__ultimoOrg, deslocamento))
						else:
							self.rótulosUsados[próximoSímbolo.valor.upper()] = [(len(self.__trechos[self.__ultimoOrg]), self.__linha, tamanhoDoOperando, self.__ultimoOrg, deslocamento),]
						for byte in range(tamanhoDoOperando):
							self.__atualizaTrechos(0)
							
					
							
					elif próximoSímbolo.tipo == "Numeral":						
						if operTipo == "#":
							self.registraOperandoImediato()
						else:
							self.registraOperando()
						try:
							bytesNumeral = próximoSímbolo.valor.to_bytes(tamanhoDoOperando, self.ordemDosBytes)
						except OverflowError:
							raise ExceçãoValorForaDosLimites(linha = self.__linha,
															 valor = próximoSímbolo.valor, )
						for byte in bytesNumeral:
							self.__atualizaTrechos(int(byte))
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
													 self.__posiçãoNoTexto - 2,
													 "Era esperado um operando, mas foi recebido %s.",
													 próximoSímbolo)
				else:					
					self.registraOperador(0)
					self.__atualizaTrechos(opcode)
					
			elif tipo == "Diretiva":				
				if esperaLinha == True:
					raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto - 1,
												"uma %s deve ser definido em uma linha sem outros operadores ou diretivas.",
												 simb)		
				esperaLinha = True				
				próximoSímbolo = self.__lêSímbolo()
				
				if sval == "ORG":
					if próximoSímbolo.tipo == "Numeral":
						if próximoSímbolo.valor in self.__trechos.keys():
							raise ExceçãoUsoIncorretoDaMemória(mensagem = "Definição repetida de ORG na posição %s" % próximoSímbolo.valor,
															posiçãoInicial=próximoSímbolo.valor)
						else:
							self.__ultimoOrg = próximoSímbolo.valor
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
													 self.__posiçãoNoTexto-2,
													  "o operando usado na diretiva 'ORG' deve ser do tipo numeral, mas foi recebido %s.",
													  próximoSímbolo)
				elif sval == "END":
					if self.parâmetroDiretivaEnd[0] is not None:
						raise ExceçãoSímboloInesperado(self.__linha,
													 self.__posiçãoNoTexto-2,
													  "somente um pseudo-operando END pode ser utilizado por programa.",
													 None)

					if próximoSímbolo.tipo in ["Numeral", "Rótulo"]:
						self.parâmetroDiretivaEnd = (próximoSímbolo, self.__posiçãoNoTexto - 2, self.__linha)
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
													self.__posiçãoNoTexto-2,
													"o operando usado na diretiva 'END' deve ser do tipo numeral, mas foi recebido %s.",
													próximoSímbolo)
					
				elif sval == "DS":
					tamanho = próximoSímbolo.valor
					if tamanho == 0:
						raise ExceçãoOperandoInválido("a diretiva 'DS' não pode ter 0 como operando.",
													self.__linha)
					self.registraAlocação("DS", tamanho, self.__posiçãoNoTexto-2)
					self.__atualizaTrechos([0 for i in range(tamanho)])
					
				elif sval == "DW" or sval == "DB":
					if próximoSímbolo.tipo == "Numeral":
						esperandoNúmero = True
						tamanho_alocado = self.memória.tamanhoDoEndereço if sval == "DW" else 1
						max_valor = self.memória.endereçoMáximoDeMemória if sval == "DW" else 0xFF
						while esperandoNúmero:
							if	próximoSímbolo.valor > max_valor or (
								próximoSímbolo.valor < 0):
								raise ExceçãoOperandoInválido("a diretiva 'DW' deve receber um operando com valor entre 0x0000 e 0xFFFF.",
															self.__linha)
							
							self.registraAlocação(sval, tamanho_alocado, self.__posiçãoNoTexto - 2)
							self.__atualizaTrechos([int(elem) for elem in próximoSímbolo.valor.to_bytes(tamanho_alocado, self.memória.ordemDosBytes)])
	
							la = self.__lookahead()
							if la and la.tipo == ",":
								if self.__lookahead(2).tipo not in ("Numeral", ):
									self.__lêSímbolo() #Consome a vírgula 
									break					
								próximoSímbolo = self.__lêSímbolo(2)
								esperandoNúmero = True
							else:
								esperandoNúmero = False
					elif (próximoSímbolo.tipo == "Rótulo") and (sval == "DW"):
						tamanhoDoOperando = self.tamanhoDoEndereço
						deslocamento = 0
						tamanhoDoTrecho = len(self.__trechos[self.__ultimoOrg]) if self.__ultimoOrg in self.__trechos else 0
						if próximoSímbolo.valor.upper() in self.rótulosUsados:
							self.rótulosUsados[próximoSímbolo.valor.upper()].append((tamanhoDoTrecho, self.__linha, tamanhoDoOperando, self.__ultimoOrg, deslocamento))
						else:
							self.rótulosUsados[próximoSímbolo.valor.upper()] = [(tamanhoDoTrecho, self.__linha, tamanhoDoOperando, self.__ultimoOrg, deslocamento),]
						for byte in range(tamanhoDoOperando):
							self.__atualizaTrechos(0)

					else:
						raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto-1,
												  'a diretiva "' + sval + '" requer um numeral ou uma sequência de numerais, não %s.',
												 próximoSímbolo)
				elif sval == "STR":					
					if próximoSímbolo.tipo == "CadeiaDeCaracteres":
						texto = próximoSímbolo.valor
						self.registraAlocação("STR", len(texto)+1, self.__posiçãoNoTexto - 2)
						self.__atualizaTrechos([int(val) for val in  bytes(texto + "\0", "iso-8859-1")])
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto-1,
												  'a diretiva STR requer uma cadeia de caracteres e não %s.',
												 próximoSímbolo)
						
				else:
					raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto-1,
												  'o símbolo "%s" é inválido.',
												 próximoSímbolo) #EQU
					
			elif tipo == "DefiniçãoDeRótulo":				
				if esperaLinha == True:
					raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto -1, 
												"o %s deve ser definido em uma linha sem outros operadores ou diretivas.",
												 simb)
				if not sval.upper() in self.definiçõesDeRótulos.keys():
					índiceDoPróximoSímboloNãoWhitespace = 1
					while self.__lookahead(índiceDoPróximoSímboloNãoWhitespace).tipo == "NovaLinha":
						índiceDoPróximoSímboloNãoWhitespace += 1
					próximoSímbolo = self.__lêSímbolo(índiceDoPróximoSímboloNãoWhitespace)
					if 	próximoSímbolo.tipo in ["EOF", "NovaLinha", "DefiniçãoDeRótulo", "Numeral", "@", "#"]:
						raise ExceçãoSímboloInesperado(self.__linha,
													 self.__posiçãoNoTexto,
													"uma definição de rótulo deve ser seguida de um operador ou de uma diretiva, mas foi recebido %s.",
													 próximoSímbolo)						
					if próximoSímbolo.valor in parâmetros.diretivasSemEndereço:
						raise ExceçãoSímboloInesperado(self.__linha,
													self.__posiçãoNoTexto,
													 "uma definição de rótulo não pode ser seguida de uma diretiva que não aloca memória, mas foi recebido %s.",
													 próximoSímbolo)
					else:
						self.definiçõesDeRótulos[sval.upper()] = (self.posição, self.__linha, self.tamanhoDoEndereço)
						self.__posiçãoNoTexto -= 1
				else:
					pos = self.definiçõesDeRótulos[sval.upper()][1]
					raise ExceçãoDefiniçãoDeRótuloRepetida(linhaOriginal=pos, linha= self.__linha, valor=sval.upper())
				
			elif tipo == "EOF":
				break
			
			elif tipo == "Rótulo":
				if esperaLinha == False:		
					esperaLinha = True
				else:
					raise ExceçãoSímboloInesperado(self.__linha,
							 self.__posiçãoNoTexto -1, 
							"o %s deve ser definido em uma linha sem outros operadores ou diretivas.",
							 simb)
				aguardaEQU = self.__lêSímbolo()
				if aguardaEQU.tipo == "Diretiva" and aguardaEQU.valor == "EQU":
					aguardaNumeral = self.__lêSímbolo()
					if aguardaNumeral.tipo == "Numeral":
						if not sval.upper() in self.definiçõesDeRótulos.keys():
							tamanho = ceil(log(aguardaNumeral.valor +1, 2**8))
							self.definiçõesDeRótulos[sval.upper()] = (aguardaNumeral.valor, self.__linha, tamanho)
						else:
							pos = self.definiçõesDeRótulos[sval.upper()][1]
							raise ExceçãoDefiniçãoDeRótuloRepetida(linhaOriginal=pos, linha= self.__linha, valor=sval.upper())
					else:
						raise ExceçãoSímboloInesperado(self.__linha,
													self.__posiçãoNoTexto,
													"a diretiva deve ser seguida de um numeral, mas foi recebido %s.",
													 aguardaNumeral)
				else:
					raise ExceçãoSímboloInesperado(self.__linha,
												self.__posiçãoNoTexto,
												"um rótulo no começo de uma linha deve ser utilizado com a diretiva EQU, mas foi recebido %s.",
												aguardaEQU)
	
							
			else:				
				if esperaLinha == True:
					raise ExceçãoSímboloInesperado(self.__linha,
												 self.__posiçãoNoTexto, 
												"o %s deve ser definido em uma nova linha.",
												 simb)
				raise ExceçãoSímboloInesperado(self.__linha,
				self.__posiçãoNoTexto,
				"era esperado um operador, ou um rótulo, ou uma definição de rótulo, ou um fim de arquivo ou uma diretiva, mas foi recebido %s.",
				 simb)
		
		self.__validaRótulos()
		self.__checaViabilidadeDeORG()
		self.__adicionaCódigoÀMemória()
		self.__configuraEndereçoInicial()
			
	def __lêSímbolo(self, n=1):
		simb = self.símbolos[self.__posiçãoNoTexto + n - 1]
		self.__posiçãoNoTexto += n
		return simb
	
	def __lookahead(self, n=1):
		if len(self.símbolos) > self.__posiçãoNoTexto + n - 1:
			simb = self.símbolos[self.__posiçãoNoTexto + n - 1]
			return simb
		else:
			return False
		
	def checaAdiante(self, esperados):
		for i, simb in enumerate(esperados):
			la = self.__lookahead(1+i)
			if not la or la.tipo != simb.tipo:
				return False
		return [self.__lêSímbolo() for simb in esperados]
			
	def registraOperador(self, tamanhoDoOperando):
		self.endereçosDosOperadores += [(self.posição, self.__linha, tamanhoDoOperando)]
		
	def registraOperando(self, rótulo=None, deslocamento=0):
		self.endereçosDosOperandos += [(self.posição, rótulo, self.__linha, deslocamento)]
		
	def registraOperandoImediato(self, rótulo=None, deslocamento=0):		
		self.endereçosDosOperandosImediatos += [(self.posição, rótulo, self.__linha, deslocamento)]
		
	def registraAlocação(self, nome, tamanho, posiçãoNoTexto):
		self.endereçosDasAlocações += [((self.posição), nome, tamanho, self.__linha)]
		
	def validaNumeral(self, tamanho):
		pass
	

	def __configuraEndereçoInicial(self):
		param, pos, linha = self.parâmetroDiretivaEnd
		if param is None:
			self.end = 0
		elif param.tipo == "Numeral":
			self.end = param.valor
		elif param.tipo == "Rótulo":
			rótulo = param.valor.upper()
			if not rótulo in self.definiçõesDeRótulos.keys():
				raise ExceçãoRótuloSemDefinição(linha, pos, rótulo)
			else:
				self.end = self.definiçõesDeRótulos[rótulo][0]
		else:
			self.end = 0


	def __validaRótulos(self):
		for rótulo, listaDeRótulos in self.rótulosUsados.items():
			if not rótulo in self.definiçõesDeRótulos.keys():
				raise ExceçãoRótuloSemDefinição(listaDeRótulos[0][1], listaDeRótulos[0][0], rótulo)
			for pos, linha, tamanho, org, deslocamento in listaDeRótulos:
				if tamanho < self.definiçõesDeRótulos[rótulo][2]:
					raise ExceçãoValorForaDosLimites(
						valor = self.definiçõesDeRótulos[rótulo][0],
						linha = linha
					)
				endereçoDaDefinição = (self.definiçõesDeRótulos[rótulo][0] + deslocamento) % self.memória.valorMáximoDoEndereço
				
				self.__trechos[org][pos: pos + tamanho] = endereçoDaDefinição.to_bytes(self.tamanhoDoEndereço, self.ordemDosBytes)
					

	def __adicionaCódigoÀMemória(self):
		for chave in self.__trechos.keys():
			self.memória[chave: chave+ len(self.__trechos[chave])] = self.__trechos[chave]
		
	def __atualizaTrechos(self, entrada):
		if not self.__ultimoOrg in self.__trechos.keys():
			self.__trechos[self.__ultimoOrg] = []
		if isinstance(entrada, int):
			self.__trechos[self.__ultimoOrg].append(entrada)
		elif isinstance(entrada, list):
			self.__trechos[self.__ultimoOrg] += entrada
		else:
			raise Exception()
		
	def __checaViabilidadeDeORG(self):
		chavesOrdenadas = sorted(self.__trechos.keys())
		últimaPosição = 0
		for i in range(len(chavesOrdenadas)):
			últimaPosição = len(self.__trechos[chavesOrdenadas[i]]) + chavesOrdenadas[i]
			if not i + 1 < len(chavesOrdenadas):
				if últimaPosição > self.memória.endereçoMáximoDeMemória:
					raise ExceçãoUsoIncorretoDaMemória("A seção que começa na posição de memória %s ultrapassa o valor máximo da memória.",
													chavesOrdenadas[i],
													self.memória.endereçoMáximoDeMemória)
			else:
				if últimaPosição > chavesOrdenadas[i + 1]:
					raise ExceçãoUsoIncorretoDaMemória("A seção que começa na posição de memória %s colide com a seção iniciada em %s.",
													chavesOrdenadas[i],
													chavesOrdenadas[i + 1])
	
	@property
	def posição(self):
		if not self.__ultimoOrg in self.__trechos.keys():
			tamanho = 0
		else:
			tamanho = len(self.__trechos[self.__ultimoOrg])
		return tamanho + self.__ultimoOrg
	
	