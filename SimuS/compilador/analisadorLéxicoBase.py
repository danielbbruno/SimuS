
import re
from SimuS.compilador.exceções import ExceçãoSímboloInválido
from SimuS.simulador import parâmetros

class AnalisadorLéxicoBase:
	def __init__(self, texto):
		self.__padrõesCompiladosDeSímbolos = self.__geraPadrõesCompiladosDeSímbolos()
		self.__padrõesCompiladosDeComentários = self.__geraPadrõesCompilados(self.comentarios)
		self.__padrõesCompiladosDeSeparadores = self.__geraPadrõesCompilados(self.separadores)
		self.__recebeSímboloDoGrupo = re.compile("tok([0-9]+)sep([0-9]+)").match
		self.textoNãoProcessado = texto
		
	def __geraPadrõesCompilados(self, vocabulárioEntrada):
		padrão = ""
		for i, (subPadrão, ação, args) in enumerate(vocabulárioEntrada):
			assert (callable(ação) or ação == None)
			c = re.compile(subPadrão)
			assert c.groups == 0
			padrão = padrão + "(" + subPadrão + ")|"
		padrão = padrão[:-1]
		padrãoCompilado = re.compile(padrão)
		return padrãoCompilado
	
	def __geraPadrõesCompiladosDeSímbolos(self):
		padrão = ""
		voc = self.vocabulário
		sep = self.separadores
		for i, (subPadrãoDeSímbolo, açãoDoSímbolo, args) in enumerate(voc):
			for j, (subPadraoDeSeparador, acaoDoSeparador, args) in enumerate(sep):
				assert (callable(açãoDoSímbolo) or açãoDoSímbolo == None)				
				assert (callable(acaoDoSeparador) or acaoDoSeparador == None)
				c = re.compile(subPadrãoDeSímbolo)
				assert c.groups == 0
				c = re.compile(subPadraoDeSeparador)
				assert c.groups == 0
				padrão = padrão + "(?P<tok" + str(i) + "sep" + str(j) + ">(" + subPadrãoDeSímbolo + ")(" + subPadraoDeSeparador + "))|"
		padrão = padrão[:-1]
		padraoCompilado = re.compile(padrão)
		return padraoCompilado
		
	def buscaPróximoSímbolo(self):
		novaCombinacao = self.__padrõesCompiladosDeSímbolos.match(self.textoNãoProcessado)
		if novaCombinacao:
			símboloId, separadorId = self.__recebeSímboloDoGrupo(novaCombinacao.lastgroup).groups()			
			grupoSímbolo = self.__padrõesCompiladosDeSímbolos.groupindex[novaCombinacao.lastgroup] + 1
			grupoSeparador = self.__padrõesCompiladosDeSímbolos.groupindex[novaCombinacao.lastgroup] + 2
			palavraSímbolo = novaCombinacao.groups()[grupoSímbolo - 1]
			palavraSeparador = novaCombinacao.groups()[grupoSeparador - 1]			
			subpadraoDoSímbolo, acaoDoSímbolo, tokArgs = self.vocabulário[int(símboloId)]
			subpadraoDoSeparador, acaoDoSeparador, sepArgs = self.separadores[int(separadorId)]
			i, j = novaCombinacao.span()
			self.textoNãoProcessado = self.textoNãoProcessado[j:]
			valorDeRetorno = []
			if acaoDoSímbolo:
				valorDeRetorno.append(acaoDoSímbolo(palavraSímbolo, *tokArgs))
			else:
				valorDeRetorno.append((True,))
			if acaoDoSeparador:
				valorDeRetorno.append(acaoDoSeparador(palavraSeparador, *sepArgs))
			else:
				valorDeRetorno.append((True,))
			return valorDeRetorno
		return False, False
	
	def buscaPróximoSeparador(self):
		novaCombinação = self.__padrõesCompiladosDeSeparadores.match(self.textoNãoProcessado)
		if novaCombinação:
			idDaRegra = novaCombinação.lastindex
			subpadrão, ação, args = self.separadores[idDaRegra - 1]
			i, j = novaCombinação.span()
			palavra = self.textoNãoProcessado[:j]
			self.textoNãoProcessado = self.textoNãoProcessado[j:]
			if ação != None:
				valorDeRetorno = ação(palavra, *args)
			else:
				valorDeRetorno = (True,)
			return valorDeRetorno
		return False
	
	def geraTextoSimplificado(self):
		textoSimplicado = ""
		for i, simb in enumerate(self.símbolosProcessados):
			if i > 0:
				if self.símbolosProcessados[i - 1].geraTextoSimplificado() != "\n":
					textoSimplicado += " " + simb.geraTextoSimplificado()
				else:
					textoSimplicado += simb.geraTextoSimplificado()
			else:
				textoSimplicado += simb.geraTextoSimplificado()
		return textoSimplicado
	
	def iniciaAnálise(self):
		self.símbolosProcessados = []
		self.textoNãoProcessado = self.__padrõesCompiladosDeComentários.sub("", self.textoNãoProcessado)
		self.estado = u"em execução"
		tuplaPróximoSeparador = True
		
		while tuplaPróximoSeparador and (not self.estado == u"finalizado"):
			tuplaPróximoSeparador = self.buscaPróximoSeparador()
			if tuplaPróximoSeparador:
				for separador in tuplaPróximoSeparador:
					if isinstance(separador, Símbolo):
						self.símbolosProcessados.append(separador)
				
		while self.textoNãoProcessado != "" and (not self.estado == u"finalizado"):
			tuplaPróximoSímbolo, tuplaPróximoSeparador = self.buscaPróximoSímbolo()
			if not tuplaPróximoSeparador or not tuplaPróximoSímbolo:
				linha = 0
				for elem in self.símbolosProcessados:
					if elem == Símbolo("NovaLinha"):
						linha += 1
				símboloInválido = re.compile("[\S]+(?=(\s|$))")
				encaixe = símboloInválido.match(self.textoNãoProcessado)
				if encaixe:
					raise ExceçãoSímboloInválido(linha=linha,
												pos=len(self.símbolosProcessados),
												tipo="Rótulo",
												valor=encaixe.group(),)
				else:					
					raise ExceçãoSímboloInválido(linha=linha,
												pos=len(self.símbolosProcessados),
												tipo=None,
												valor=None,)
			for símbolo in tuplaPróximoSímbolo:
				self.símbolosProcessados.append(símbolo)
			for separador in tuplaPróximoSeparador:
				if isinstance(separador, Símbolo):
					self.símbolosProcessados.append(separador)
				
			while tuplaPróximoSeparador and (not self.estado == u"finalizado"):			
				tuplaPróximoSeparador = self.buscaPróximoSeparador()
				if tuplaPróximoSeparador:
					for separador in tuplaPróximoSeparador:
						if isinstance(separador, Símbolo):
							self.símbolosProcessados.append(separador)
							
		if self.estado == u"finalizado":
			self.textoNãoProcessado = ""

class Símbolo:
	def __init__(self, tipo, valor=None, anotações=None, linha=None, pos=None):
		self.tipo = tipo
		self.valor = valor
		self.linha = linha
		self.posiçãoNoTexto = pos
		self.anotações = anotações
		if self.tipo == "Operador":
			if self.valor not in parâmetros.operandos:
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo == "Diretiva":
			if self.valor not in parâmetros.diretivas:
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo == "Numeral":
			if type(self.valor) is not int:
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo == "CadeiaDeCaracteres":
			if type(self.valor) is not str:
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo == "Rótulo":
			if ((type(self.valor) is not str) 
			or (self.valor in parâmetros.operandos) 
			or (self.valor in parâmetros.diretivas)):
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo == "DefiniçãoDeRótulo":
			if ((type(self.valor) is not str)
			or (self.valor in parâmetros.operandos) 
			or (self.valor in parâmetros.diretivas)):
				raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)
		elif self.tipo in [",", "@", "#", "+", "EOF", "NovaLinha"]:
			pass
		else:
			raise ExceçãoSímboloInválido(self.linha, self.posiçãoNoTexto, self.tipo, self.valor)

	def __repr__(self):
		if self.valor != None:
			return "<" + str(self.tipo) + ": '" + str(self.valor) + "'>"
		else:
			return "<Símbolo: '"+ str(self.tipo) + "'>"
		
	def __str__(self):
		if self.tipo in ["@", "#", ",", "+"]:
			return "símbolo " + str(self.tipo)
		else:
			if self.tipo == "Operador":
				return "operador '" + str(self.valor) + "'"
			elif self.tipo  == "Diretiva":
				return "diretiva '" + str(self.valor) + "'"
			elif self.tipo  == "NovaLinha":
				return "quebra-de-linha"
			elif self.tipo  == "EOF":
				return "fim de arquivo"
			elif self.tipo  == "Rótulo":
				return "rótulo com nome '" + str(self.valor) + "'"
			elif self.tipo  == "DefiniçãoDeRótulo":
				return "definição de rótulo com nome '" + str(self.valor) + "'"
			elif self.tipo == "Numeral":
				return "numeral com valor '" + str(self.valor) + "'" 
			else:
				return self.__repr__()
			
	def geraTextoSimplificado(self):
		if self.tipo in ("Rótulo", "Diretiva", "Operador"):
			texto = self.valor
		elif self.tipo == "Numeral":
			if self.anotações is not None:
				texto = self.anotações
			else:
				texto = str(self.valor)
		elif self.tipo in ["@", "#", ",", "+"]:
			texto = self.tipo
		elif self.tipo in ("EOF", "DefiniçãoDeRótulo"):
			texto = ""
		elif self.tipo == "NovaLinha":
			texto = "\n"
		else:
			texto = self.valor
		return str(texto)
	
	def __eq__(self, other):
		if not isinstance(other, Símbolo): return False
		return ((other.valor == self.valor) and (self.tipo.upper() == other.tipo.upper()))
