
all: icones designer build

build:
	python setup.py bdist_wheel

icones:
	./scripts/geraBinárioDeÍcones.sh

designer:
	./scripts/geraIU

clean:
	rm -rf ./pacotesExternos/feather/icons/*.png
	rm -rf SimuS/interfaceDeUsuário/íconesFeatherBin.rcc
	rm -rf ./*.egg-info
	rm -rf ./dist
	rm -rf ./build

novo_ambiente:
	rm -rf ./venv
	virtualenv venv 
	source venv/bin/activate && pip install dist/SimuS-1.0.0-py3-none-any.whl
	source venv/bin/activate && simus16

all:
	python3 setup.py bdist_wheel