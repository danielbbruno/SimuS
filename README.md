# SimuS16

SimuS16 é um simulador da arquitetura de computador Sapiens, voltada para fins didáticos.
Essa implementação é feita em python3, e utiliza Qt5/PyQt5 para a interface gráfica. 


<div align="center">
![SimuS16](./doc/printSimus.png){width=50% height=50%}
</div>

## Instalação

1. Baixe o pacote de instalação, com extensão `.whl`, da versão mais recente do SimuS16 [nessa página](https://gitlab.com/danielbbruno/SimuS/-/releases).

2. Instale o pacote

    ```shell
    pip install SimuS-<número da versão>-py3-none-any.whl
    ```
Para instalar sem permissões de super-usuário acrescente o parâmetro `--user` ao comando acima.

3. Após a instalação, o programa poderá ser executado pela linha de comando com `simus16`.
